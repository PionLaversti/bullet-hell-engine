#pragma once

#include <SDL_mixer.h>

class SoundWrapper
{
	public:
	static void Initialize();
	static bool PlayMusic( Mix_Music* musicFile );
	static void PauseMusic();
	static void ResumeMusic();
	static void StopMusic();

	static bool PlaySE( Mix_Chunk* soundEffect );

	private:
	static int maxChannels;
	static int lastChannel;
};