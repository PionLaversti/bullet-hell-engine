#include "Laser_Programmable.h"
#include "LuaWrapper_ProgrammableLaser.h"

using namespace GameObjects::Projectiles;
using namespace LuaScripting;

void LuaWrapper_ProgrammableLaser::ExposeFunctions( lua_State* script )
{
	luabridge::getGlobalNamespace( script )
		.beginClass<ProgrammableLaser>( "ProgrammableLaser" )
			.addProperty( "CurrentX", &ProgrammableLaser::GetStartX, &ProgrammableLaser::SetX )
			.addProperty( "CurrentY", &ProgrammableLaser::GetStartY, &ProgrammableLaser::SetY )
			.addProperty( "Angle", &ProgrammableLaser::GetAngle, &ProgrammableLaser::SetAngle )
			.addProperty( "AngleAcceleration", &ProgrammableLaser::GetAngAccel, &ProgrammableLaser::SetAngAccel )
			.addProperty( "Width", &ProgrammableLaser::GetWidth, &ProgrammableLaser::SetWidth )
			.addProperty( "Length", &ProgrammableLaser::GetLength, &ProgrammableLaser::SetLength )
			.addProperty( "XSpeed", &ProgrammableLaser::GetXSpeed, &ProgrammableLaser::SetXSpeed )
			.addProperty( "YSpeed", &ProgrammableLaser::GetYSpeed, &ProgrammableLaser::SetYSpeed )
			.addProperty( "XAccel", &ProgrammableLaser::GetXAccel, &ProgrammableLaser::SetXAccel )
			.addProperty( "YAccel", &ProgrammableLaser::GetYAccel, &ProgrammableLaser::SetYAccel )
			.addProperty( "XMaxSpeed", &ProgrammableLaser::GetXMaxSpeed, &ProgrammableLaser::SetXMaxSpeed )
			.addProperty( "YMaxSpeed", &ProgrammableLaser::GetYMaxSpeed, &ProgrammableLaser::SetYMaxSpeed )
			.addFunction( "SetXYSpeed", &ProgrammableLaser::SetXYSpeed )
			.addFunction( "SetXYAccel", &ProgrammableLaser::SetXYAccel )
			.addFunction( "SetXYMaxSpeed", &ProgrammableLaser::SetXYMaxSpeed )
			.addProperty( "Graphic", &ProgrammableLaser::GetGraphic, &ProgrammableLaser::SetGraphic )
			.addProperty( "Lifetime", &ProgrammableLaser::GetLifetime, &ProgrammableLaser::SetLifetime )
			.addProperty( "WarningTime", &ProgrammableLaser::GetWarningTime, &ProgrammableLaser::SetWarningTime )
		.endClass( );
}