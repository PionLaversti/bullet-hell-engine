#include "LuaFunctions.h"
#include "LuaScript.h"

using namespace LuaScripting;

LuaScript::LuaScript( std::string scriptFileName )
{
	script = LuaFunctions::OpenScript( scriptFileName );
}

void LuaScript::Preprocessing( )
{
	LuaFunctions::CallFunction( script, "Preprocessing" );
}
void LuaScript::Initialize( )
{
	LuaFunctions::CallFunction( script, "Initialize" );
}
void LuaScript::Update( )
{
	LuaFunctions::CallFunction( script, "Update" );
}
void LuaScript::Render( )
{
	LuaFunctions::CallFunction( script, "Render" );
}
void LuaScript::ShutDown( )
{
	LuaFunctions::CallFunction( script, "ShutDown" );

	LuaFunctions::CloseScript( script );

	script = nullptr;
}

void LuaScript::CallFunction( const std::string functionName )
{
	LuaFunctions::CallFunction( script, functionName );
}