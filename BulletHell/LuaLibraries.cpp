#include "LuaLibraries.h"

using namespace LuaScripting;
using namespace std;

map< string, vector< std::function<void( lua_State* )> > > LuaLibraries::registeredLibraries;

void LuaLibraries::ExposeFunctions( lua_State* script )
{
	luabridge::getGlobalNamespace( script )
		.addFunction( "IncludeLibrary", &LuaLibraries::IncludeLibrary )
		.addFunction( "DefineScriptType", &LuaLibraries::RegisterScriptType );

	IncludeLibrary( "GENERAL", script );
}

void LuaLibraries::RegisterLibrary( std::function<void( lua_State* )> libraryWrapper, string libraryName )
{
	registeredLibraries[libraryName].push_back( libraryWrapper );
}

void LuaLibraries::IncludeLibrary( const string libraryName, lua_State* script )
{
	for( uint32_t ind = 0; ind < registeredLibraries[libraryName].capacity( ); ind ++ )
		registeredLibraries[libraryName][ind]( script );
}

void LuaLibraries::RegisterScriptType( const string scriptType )
{
	return;
}