#pragma once

#include "Laser.h"

namespace GameObjects
{
	namespace Projectiles
	{
		class Laser_Rotating : public Laser
		{
			public:
			Laser_Rotating( );
			Laser_Rotating( const float newX, const float newY, const float newAngle, const float newAngleAccel,
							const int newLength, const int newWidth, const int newWarningTime, const int newLifetime, ProjectileType* const lasType );
			~Laser_Rotating( );

			void Update( );

			private:
			float angAccel;
		};
	}
}