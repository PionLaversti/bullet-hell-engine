///	--	--	--	--	--	--	--	--	--	--	--	--	--	--	--	--	--	--	--	--
/// StateMachine.h
///	Manages gamestates and switches around when required.
///	--	--	--	--	--	--	--	--	--	--	--	--	--	--	--	--	--	--	--	--

#pragma once

#include "LuaInclude.h"

namespace GameSystems
{
	namespace StateMachineSystem
	{
		class GameState;
		enum GameStateType;

		class StateMachine
		{
			public: 
			static void Initialize( );
			// Changes the application's next state if needed.
			static void ChangeState();

			static bool HasStopped( );

			// State-machine methods.
			static void HandleEvents();
			static void Update();
			static void Render();
			
			private:
			static GameState* currentState;

			static int currentStateID;
			static int nextStateID;
		};
	}
}