#pragma once

#include "Bullet.h"

namespace GameObjects
{
	namespace Projectiles
	{
		class Bullet_AccelAng : public Bullet
		{
			public:
			Bullet_AccelAng( );
			Bullet_AccelAng( const float xPosStart, const float yPosStart, const float speed, const float angStart,
							 const float angAccel, ProjectileType* bulType );

			void Update( );

			private:
			float angAccel;
		};
	}
}