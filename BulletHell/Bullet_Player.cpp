#include "Bullet_Player.h"

#include "CollisionGrid.h"
#include "ProjectileType.h"

using namespace GameObjects::Projectiles;
using namespace GameSystems;

CollisionGrid* Bullet_Player::playerBulletGrid;

Bullet_Player::Bullet_Player( const float xStart, const float yStart, const float speed, const float angle, const float dmg, ProjectileType* bulType )
	: Bullet( xStart, yStart, speed, angle, bulType )
{
	damage = dmg;
}

void Bullet_Player::Update( )
{
	if( deathFramesLeft > 0 )
	{
		xSpeed = xSpeed * 0.85f;
		ySpeed = ySpeed * 0.85f;

		MoveBy( xSpeed, ySpeed );

		deathFramesLeft--;

		return;
	}

	if( deathFramesLeft == 0 )
		Destroy( );

	MoveBy( xSpeed, ySpeed );

	SDL_Rect rect = GetCollisionRect( );

	const int newLeft = CollisionGrid::GetSquarePointLiesIn( rect.x );
	const int newTop = CollisionGrid::GetSquarePointLiesIn( rect.y );
	const int newRight = CollisionGrid::GetSquarePointLiesIn( rect.x + rect.w );
	const int newBottom = CollisionGrid::GetSquarePointLiesIn( rect.y + rect.h );

	if( collisionGridTop != newTop || collisionGridBottom != newBottom || collisionGridLeft != newLeft || collisionGridRight != newRight )
	{
		playerBulletGrid->Remove( collisionGridLeft, collisionGridTop, this );

		if( collisionGridLeft != collisionGridRight )
			playerBulletGrid->Remove( collisionGridRight, collisionGridTop, this );
		if( collisionGridTop != collisionGridBottom )
			playerBulletGrid->Remove( collisionGridLeft, collisionGridBottom, this );
		if( (collisionGridLeft != collisionGridRight) && (collisionGridTop != collisionGridBottom) )
			playerBulletGrid->Remove( collisionGridRight, collisionGridBottom, this );

		if( newTop < 0 )
		{
			ShutDown( );
		}
		else
		{
			playerBulletGrid->Add( newLeft, newTop, this );

			if( collisionGridLeft != collisionGridRight )
				playerBulletGrid->Add( newRight, newTop, this );
			if( collisionGridTop != collisionGridBottom )
				playerBulletGrid->Add( newLeft, newBottom, this );
			if( (collisionGridLeft != collisionGridRight) && (collisionGridTop != collisionGridBottom) )
				playerBulletGrid->Add( newRight, newBottom, this );

			collisionGridLeft = newLeft;
			collisionGridRight = newRight;
			collisionGridBottom = newBottom;
			collisionGridTop = newTop;
		}
	}
}

void Bullet_Player::ShutDown( )
{
	deathFramesLeft = DEATH_FRAMES;

	SDL_Rect rect = GetCollisionRect( );

	const int newLeft = CollisionGrid::GetSquarePointLiesIn( rect.x );
	const int newTop = CollisionGrid::GetSquarePointLiesIn( rect.y );
	const int newRight = CollisionGrid::GetSquarePointLiesIn( rect.x + rect.w );
	const int newBottom = CollisionGrid::GetSquarePointLiesIn( rect.y + rect.h );

	playerBulletGrid->Remove( collisionGridLeft, collisionGridTop, this );

	if( collisionGridLeft != collisionGridRight )
		playerBulletGrid->Remove( collisionGridRight, collisionGridTop, this );
	if( collisionGridTop != collisionGridBottom )
		playerBulletGrid->Remove( collisionGridLeft, collisionGridBottom, this );
	if( (collisionGridLeft != collisionGridRight) && (collisionGridTop != collisionGridBottom) )
		playerBulletGrid->Remove( collisionGridRight, collisionGridBottom, this );
}