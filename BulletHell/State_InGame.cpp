#include <boost/lexical_cast.hpp>

#include "Application.h"
#include "Constants.h"
#include "Input.h"
#include "Texture.h"
#include "Window.h"

#include "GameState.h"
#include "State_InGame.h"

#include "OverlayManager.h"

#include "ActorManager.h"
#include "Enemy.h"
#include "Player.h"

#include "CollisionDetector.h"

#include "ProjectileManager.h"
#include "Bullet_Player.h"

#include "Stage.h"

#include "LuaFunctions.h"


using namespace GameObjects;
using namespace GameObjects::Projectiles;
using namespace GameSystems;
using namespace GameSystems::StateMachineSystem;

struct buldata
{
	std::string alias;
	int collisionWidth;
	int collisionHeight;
	SDL_Rect graphicRect;
	bool rotate;
	std::string alphaBlend;
};

State_InGame::State_InGame(const int _width = DEFAULT_WIDTH, const int _height = DEFAULT_HEIGHT) : GameState( )
{
	ActorManager::SpawnPlayer( "player.lua" );

	stage = new Stage( "main.lua" );
	stage->Preprocessing( );

	levelWidth	= _width;
	levelHeight	= _height;
	bulletBufferDistance	= DEFAULT_BULLET_BUFFER_DIST;
	playerBufferDistance	= DEFAULT_PLAYER_BUFFER_DIST;
	
	frameCount = 0;

	nextState = STATE_NULL;

	playerMoveLeft = false;
	playerMoveRight = false;
	playerMoveUp = false;
	playerMoveDown = false;
	playerFocus = false;
	playerInvincibility = false;

	paused = false;

	ProjectileManager::Initialize( );
	CollisionDetector::Initialize( _DEFAULT_SCREEN_WIDTH, _DEFAULT_SCREEN_HEIGHT );

	stage->Initialize( );

	ActorManager::Initialize( );
	InputManager::Initialize( );
	OverlayManager::Initialize( );
}
State_InGame::~State_InGame()
{
	ActorManager::ShutDown( );
	OverlayManager::ShutDown( );
	ProjectileManager::ShutDown( );
}

void State_InGame::HandleEvents()
{
	static SDL_Event l_event;

	InputManager::Update( );

	playerMoveUp =		(InputManager::KeyPressed( SDL_SCANCODE_UP ) || InputManager::KeyDown( SDL_SCANCODE_UP ));
	playerMoveDown =	(InputManager::KeyPressed( SDL_SCANCODE_DOWN ) || InputManager::KeyDown( SDL_SCANCODE_DOWN ));
	playerMoveLeft =	(InputManager::KeyPressed( SDL_SCANCODE_LEFT ) || InputManager::KeyDown( SDL_SCANCODE_LEFT ));
	playerMoveRight =	(InputManager::KeyPressed( SDL_SCANCODE_RIGHT ) || InputManager::KeyDown( SDL_SCANCODE_RIGHT ));
	playerFocus =		(InputManager::KeyPressed( SDL_SCANCODE_LSHIFT ) || InputManager::KeyDown( SDL_SCANCODE_LSHIFT ));
	playerShoot =		(InputManager::KeyPressed( SDL_SCANCODE_Z ) || InputManager::KeyDown( SDL_SCANCODE_Z ));
	playerBomb =		(InputManager::KeyPressed( SDL_SCANCODE_X ) || InputManager::KeyDown( SDL_SCANCODE_X ));

	playerInvincibility = (InputManager::KeyPressed( SDL_SCANCODE_I ) || InputManager::KeyDown( SDL_SCANCODE_I ));

	if( InputManager::KeyPressed( SDL_SCANCODE_P ) )
		paused = !paused;

	if( SDL_PollEvent( &l_event ) )
	{
		if( l_event.type == SDL_QUIT )
			nextState = STATE_EXIT;
	}
}
void State_InGame::HandleLogic()
{
	if( paused )
		return;

	ActorManager::Update( );
	OverlayManager::Update( );
	ProjectileManager::Update();
	stage->Update( );

	std::vector<Bullet_Player*> overlap;
	std::vector<Bullet_Player*>::const_iterator overlapIter;
	Bullet_Player* playerBullet;

	for( 
		std::list<Enemy*>::iterator enemyIterator = ActorManager::enemies.begin( );
		enemyIterator != ActorManager::enemies.end( );
		enemyIterator++
		)
	{
		if( (*enemyIterator) != nullptr &&
			(*enemyIterator)->IsActive( ) )
		{
			overlap = CollisionDetector::GetBulletsHittingEnemy( (*enemyIterator)->GetCollisionRect( ) );

			for(
				overlapIter = overlap.begin( );
				overlapIter != overlap.end( );
				overlapIter++
				)
			{
				playerBullet = (*overlapIter);

				(*enemyIterator)->Damage( playerBullet->GetDamage( ) );
				playerBullet->ShutDown( );
			}
		}
	}

	Player* player = ActorManager::activePlayer;

	if( !player->IsInvincible() )
	{
		if( CollisionDetector::IsPlayerHitByBullet( player->GetCollisionRect() ) ||
			CollisionDetector::Intersects( &ProjectileManager::enemyLaserList, player->GetCollisionRect() ))
		{
			ProjectileManager::Reset();

			frameCount = 0;
		}
	}

	if( playerBomb )
	{
		player->Bomb();
		playerBomb = false;
	}
	if( playerShoot )
	{
		player->Shoot();
	}
	if( playerMoveUp && player->GetY() > 16 )
		player->MoveUp();
	if( playerMoveDown && player->GetY() < 424  )
		player->MoveDown();
	if( playerMoveRight && player->GetX() < 592  )
		player->MoveRight();
	if( playerMoveLeft && player->GetX() > 16 )
		player->MoveLeft();

	player->SetSlowMove(playerFocus);
	player->SetInvincibility( playerInvincibility );

	frameCount++;
}
void State_InGame::Render()
{
	stage->RenderBackground( );
	ActorManager::Render( );
	ProjectileManager::Render( );
	OverlayManager::Render( );
}

void State_InGame::ResizeLevelSize( const int _w, const int _h )
{
	levelWidth	= _w;
	levelHeight	= _h;
}
void State_InGame::ResizeLevelBounds_Bullets( const int _size )
{
	bulletBufferDistance = _size;
}
void State_InGame::ResizeLevelBounds_Player( const int _size )
{
	playerBufferDistance = _size;
}