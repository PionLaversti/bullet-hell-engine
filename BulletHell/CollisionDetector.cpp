#include "Bullet.h"
#include "Bullet_Player.h"
#include "Laser.h"

#include "CollisionDetector.h"
#include "CollisionGrid.h"
#include "ProjectileManager.h"

#include <boost\lexical_cast.hpp>

using namespace GameObjects;
using namespace GameObjects::Projectiles;
using namespace GameSystems;
using namespace std;

CollisionGrid CollisionDetector::enemyBulletCollisionGrid;
CollisionGrid CollisionDetector::playerBulletCollisionGrid;

void CollisionDetector::Initialize( const int screenWidth, const int screenHeight )
{
	enemyBulletCollisionGrid.Initialize( screenWidth, screenHeight );
	playerBulletCollisionGrid.Initialize( screenWidth, screenHeight );

	Bullet::enemyBulletGrid			= &enemyBulletCollisionGrid;
	Bullet_Player::playerBulletGrid = &playerBulletCollisionGrid;
}

// Checks to see if a rectangle intersects with any bullets.
// Will choose appropriate method of collision detection.
// Right now just uses brute-force AABB checking.
bool CollisionDetector::Intersects( const std::vector<Bullet*> * bulletList, const SDL_Rect _target )
{
	vector<Bullet*>::const_iterator iter = bulletList->begin( );

	while( iter != bulletList->end( ) )
	{
		if( *iter != nullptr && (*iter)->IsActive( ) )
			if( Intersects_AABB( (*iter), _target ) )
				return true;

		iter++;
	}

	return false;
}

bool CollisionDetector::IsPlayerHitByBullet( const SDL_Rect target )
{
	if( enemyBulletCollisionGrid.HasObjectsInOverlappedSquares( target ) )
	{
		vector<GameObject*> bullets = enemyBulletCollisionGrid.GetObjectsInOverlappedSquares( target );

		vector<GameObject*>::const_iterator iter = bullets.begin( );

		while( iter != bullets.end( ) )
		{
			if( *iter != nullptr && (*iter)->IsActive( ) )
				if( Intersects_AABB( (Bullet*) (*iter), target ) )
					return true;

			iter++;
		}
	}

	return false;
}

vector<Bullet_Player*> CollisionDetector::GetBulletsHittingEnemy( const SDL_Rect target )
{
	vector<Bullet_Player*> collisions;

	if( playerBulletCollisionGrid.HasObjectsInOverlappedSquares( target ) )
	{
		vector<GameObject *> bullets = playerBulletCollisionGrid.GetObjectsInOverlappedSquares( target );

		vector<GameObject *>::const_iterator iter = bullets.begin( );

		while( iter != bullets.end( ) )
		{
			if( *iter != nullptr && (*iter)->IsActive( ) )
				if( Intersects_AABB( (Bullet_Player*) (*iter), target ) )
					collisions.push_back( (Bullet_Player*) (*iter) );

			iter++;
		}
	}
	
	return collisions;
}

// Checks to see if a rectangle intersects with any lasers.
// Uses closest distance to line segment calculations.
bool CollisionDetector::Intersects( const std::vector<Laser *>* laserList, const SDL_Rect _target )
{
	vector<Laser*>::const_iterator iter = laserList->begin( );

	while( iter != laserList->end( ) )
	{
		if( *iter != nullptr && (*iter)->IsActive( ) && (*iter)->IsHitboxActive( ) )
			if( Intersects_Linear( (*iter), _target ) )
				return true;

		iter++;
	}

	return false;
}

std::vector<Bullet*> CollisionDetector::GetBulletCollisions( const std::vector<GameObjects::Projectiles::Bullet*> * bulletList, const SDL_Rect target )
{
	vector<Bullet*>::const_iterator iter = bulletList->begin( );
	vector<Bullet*> collisions;

	while( iter != bulletList->end( ) )
	{
		if( *iter != nullptr && (*iter)->IsActive( ) )
			if( Intersects_AABB( (*iter), target ) )
				collisions.push_back( (*iter) );

		iter++;
	}

	return collisions;
}
	
	// Checks to see if any axis-aligned bounding boxes overlap.
bool CollisionDetector::Intersects_AABB( GameObjects::Projectiles::Bullet * object, const SDL_Rect target )
{
	SDL_Rect objRect = object->GetCollisionRect( );

	return(abs( objRect.x - target.x ) * 2 < (objRect.w + target.w) &&
			abs( objRect.y - target.y ) * 2 < (objRect.h + target.h));
}

	// Checks the shortest distance between a line and a box's center.
	//	If it's less than a specified value, return true.
bool CollisionDetector::Intersects_Linear( Laser* _las, const SDL_Rect _target )
{
		// Starting point of the line.
	float _x1, _y1;
		// End point of the line.
	float _x2, _y2;
		// Center of the box.
	float _x3, _y3;

	_x1 = _las->GetStartX();
	_y1 = _las->GetStartY();
	_x2 = _x1 + (_las->GetLength() * (float) cos(_las->GetAngle() * (M_PI / 180)));
	_y2 = _y1 + (_las->GetLength() * (float) sin(_las->GetAngle() * (M_PI / 180)));
	_x3 = (float) _target.x + (_target.w / 2);
	_y3 = (float) _target.y + (_target.h / 2);

		// The non-absolute length of the laser along the horizontal axis.
	float dist_x = _x2 - _x1;
		// The non-absolute length of the laser along the vertical axis.
	float dist_y = _y2 - _y1;

	float dist_sq = (dist_x * dist_x) + (dist_y * dist_y);

		// Ratio of distance along the line, compared to the line's length.
	float comp_dist = (((_x3 - _x1) * dist_x) + ((_y3 - _y1) * dist_y)) / dist_sq;

	if( comp_dist > 1 )
		comp_dist = 1;
	else if( comp_dist < 0 )
		comp_dist = 0;

		// Closest point on the line to the box.
	float _xpos, _ypos;

	_xpos = _x1 + (comp_dist * dist_x);
	_ypos = _y1 + (comp_dist * dist_y);

		// Distance along the horizontal axis between the chosen point and box's center.
	float diff_x = _xpos - _x3;
		// Distance along the vertical axis between the chosen point and box's center.
	float diff_y = _ypos - _y3;

	dist_sq = (diff_x * diff_x) + (diff_y * diff_y);
	
		// If it's less than half the laser's length + half the target's width, it's a hit.
		// This is a fair approximation.
	float base_dist = (_las->GetWidth() + _target.w) / 2.0f;
	
	if( dist_sq <= ( base_dist * base_dist ))
		return true;

	return false;
}