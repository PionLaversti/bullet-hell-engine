#pragma once

#include <string>

#include "LuaScript.h"

namespace GameObjects
{
	class Stage
	{
		public:
		Stage( std::string scriptFileName );

		void Preprocessing( );
		void Initialize( );
		void Update( );
		void RenderBackground( );
		void Render( );
		void ShutDown( );

		private:
		LuaScripting::LuaScript* script;
	};
}