#include "Lifebar.h"

#include "Enemy.h"
#include "LuaScript.h"
#include "OverlayManager.h"

using namespace GameObjects;
using namespace GameSystems;
using namespace LuaScripting;

Lifebar::Lifebar( const std::string scriptFileName )
{
	script = new LuaScript( scriptFileName );

	Preprocessing( );

	script->RegisterEntity( this, "Lifebar" );
	OverlayManager::Register( this );

	Initialize( );
}

void Lifebar::Preprocessing( )
{
	script->Preprocessing( );
}
void Lifebar::Initialize( )
{
	script->Initialize( );
}
void Lifebar::Update( )
{
	if( enemy->IsActive( ) )
	{
		currentLife = enemy->GetLife( );
		maximumLife = enemy->GetMaxLife( );

		script->Update( );
	}
	else
		ShutDown( );
}
void Lifebar::Render( )
{
	if( enemy != nullptr && enemy->IsActive( ) )
		script->Render( );
}
void Lifebar::ShutDown( )
{
	script->ShutDown( );

	OverlayManager::Unregister( this );
}