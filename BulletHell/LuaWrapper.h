#pragma once

namespace LuaScripting
{
	class LuaWrapper
	{
		public:
		static bool Initialize( );

		private:
		static void InitializeLibraries( );
	};
}