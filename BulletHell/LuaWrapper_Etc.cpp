#include "LuaWrapper_Etc.h"

#include "RandomNumberGenerator.h"

using namespace GameSystems;
using namespace LuaScripting;

void LuaWrapper_Etc::ExposeFunctions( lua_State* script )
{
	luabridge::getGlobalNamespace( script )
		.addFunction( "Random", &RandomNumberGenerator::RandomNumber )
		.addFunction( "SeedRNG", &RandomNumberGenerator::SeedRNG );
}