#include "Texture.h"

#include "GLWrapper.h"
#include "DrawData.h"
#include "Window.h"

using namespace GameObjects;

Texture::Texture()
{
		// Init texture ID
	textureID = 0;

		// Init texture dimensions
	textureWidth = 0;
	textureHeight = 0;
}

Texture::~Texture()
{
		// Free it if needed
	FreeTexture();
}

bool Texture::LoadTextureFromFile( std::string _filename )
{
		// Free the texture if it exists.
	FreeTexture();

		// Surface with the loaded image.
	SDL_Surface *tempSurface;
	GLenum textureFormat;
	GLenum error;
	GLint numColours;

	tempSurface = IMG_Load( _filename.c_str() );

	if(tempSurface == nullptr )	
		return false;

	textureWidth  = tempSurface->w;
	textureHeight = tempSurface->h;

		// Check dimensions. Best if they're powers of 2.
	if( ( tempSurface->w & (tempSurface->w - 1)) != 0 )
		printf( "WARNING: Image's width is not a power of 2.\n" );
	if( ( tempSurface->h & (tempSurface->h - 1)) != 0 )
		printf( "WARNING: Image's height is not a power of 2.\n" );

		// Get data from the SDL surface.
	numColours = tempSurface->format->BytesPerPixel;

		// Has alpha channel
	if( numColours == 4 )
	{
		if( tempSurface->format->Rmask == 0xff )
			textureFormat = GL_RGBA;
		else
			textureFormat = GL_BGRA;
	}
		// Has NO alpha channel
	else if( numColours == 3 )
	{
		if( tempSurface->format->Rmask == 0xff )
			textureFormat = GL_RGB;
		else
			textureFormat = GL_BGR;
	}
	else
		printf( "WARNING: Image isn't truecolour. Prepare your eyes for BLEED." );

		// Generate texture ID.
	glGenTextures( 1, &textureID );
		// Bind texture ID.
	glBindTexture( GL_TEXTURE_2D, textureID );

		// Control how texture is handled when magnified in/out.
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );

		// FINALLY generate texture.
		/*
		 * Arg1 : Texture target / Type of texture.
		 * Arg2 : Mip-map level.
		 * Arg3 : Pixel-format of the texture you'll store.
		 * Arg4 : Texture width
		 * Arg5 : Texture height
		 * Arg6 : Texture border width
		 * Arg7 : Pixel-format of the pixel data you're passing to it.
		 * Arg8 : Data-type for the pixel data you're passing to it.
		 * Arg9 : Pointer address of the pixel data you're assigning to it.
		 */
	glTexImage2D( GL_TEXTURE_2D, 0, numColours, tempSurface->w, tempSurface->h, 0, textureFormat, GL_UNSIGNED_BYTE, tempSurface->pixels );

	error = glGetError();

	if( textureWidth == 0 || textureHeight == 0 )
		return false;

		// Unbind the pixel data so that it doesn't screw with rendering.
	glBindTexture( GL_TEXTURE_2D, NULL );

		// Release the surface now that we've got what we've needed.
	SDL_FreeSurface( tempSurface );

	if( error != GL_NO_ERROR )
	{
		printf( "Error loading texture from %s file!\n", _filename );
		return false;
	}

	return true;
}

void Texture::FreeTexture()
{
	if( textureID != 0 )
	{
		glDeleteTextures( 1, &textureID );
		textureID = 0;
	}

	textureWidth = 0;
	textureHeight = 0;
}

void Texture::Render( GLfloat X, GLfloat Y )
{
	RenderToSize( X, Y, textureWidth, textureHeight );
}

void Texture::RenderToSize( GLfloat X, GLfloat Y, GLfloat width, GLfloat height )
{
	assert( textureID != 0 );

		// Remove previous transforms.
	glLoadIdentity();

		// Translate to rendering point.
	glTranslatef( X, Y, 0.0f );

		// Bind texture to use.
	glBindTexture( GL_TEXTURE_2D, textureID );

		// Render it!
	glBegin( GL_QUADS );
	glTexCoord2f( 0.0f, 0.0f ); glVertex2f( (GLfloat) -width / 2,	(GLfloat) -height / 2 );
	glTexCoord2f( 1.0f, 0.0f );	glVertex2f( (GLfloat) width / 2,	(GLfloat) -height / 2 );
	glTexCoord2f( 1.0f, 1.0f );	glVertex2f( (GLfloat) width / 2,	(GLfloat) height / 2 );
	glTexCoord2f( 0.0f, 1.0f );	glVertex2f( (GLfloat) -width / 2,	(GLfloat) height / 2 );
	glEnd();

		// Bind texture to use.
	glBindTexture( GL_TEXTURE_2D, NULL );
}
void Texture::RenderInRect( GLfloat left, GLfloat top, GLfloat right, GLfloat bottom )
{
	assert( textureID != 0 );

	// Remove previous transforms.
	glLoadIdentity();

	// Translate to rendering point.
	glTranslatef( left, top, 0.0f );

	// Bind texture to use.
	glBindTexture( GL_TEXTURE_2D, textureID );

	// Render it!
	glBegin( GL_QUADS );
	glTexCoord2f( 0.0f, 0.0f ); glVertex2f( (GLfloat) 0,			(GLfloat) 0 );
	glTexCoord2f( 1.0f, 0.0f );	glVertex2f( (GLfloat) right - left, (GLfloat) 0 );
	glTexCoord2f( 1.0f, 1.0f );	glVertex2f( (GLfloat) right - left, (GLfloat) bottom - top );
	glTexCoord2f( 0.0f, 1.0f );	glVertex2f( (GLfloat) 0,			(GLfloat) bottom - top );
	glEnd();

	// Bind texture to use.
	glBindTexture( GL_TEXTURE_2D, NULL );
}

void Texture::Render( DrawData drawData )
{
	// Remove previous transforms.
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	SDL_Rect clipZone;
	clipZone = drawData.clipZone;

	// Translate to rendering point.
	glTranslatef( drawData.blitZone.x, drawData.blitZone.y, 0.0f );

	// Rotate around translation point if the bullet's graphic should rotate.
	if( drawData.shouldRotate )
		glRotatef( drawData.angle + 90, 0.0f, 0.0f, 1.0f );

	Window::SwitchBlendMode( drawData.blendMode );

	glTexEnvf( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE );

	glColor4f( 1.0f, 1.0f, 1.0f, drawData.alpha );

	// Bind texture to use.
	glBindTexture( GL_TEXTURE_2D, textureID );

	float halfWidth  = drawData.blitZone.w / 2.0f;
	float halfHeight = drawData.blitZone.h / 2.0f;

	float UV_left	= (float) clipZone.x					/ textureWidth;
	float UV_right	= (float) (clipZone.x + clipZone.w)	/ textureWidth;
	float UV_top	= (float) clipZone.y					/ textureHeight;
	float UV_bottom	= (float) (clipZone.y + clipZone.h)	/ textureHeight;

	// Render it!
	glBegin( GL_QUADS );

	if( drawData.shouldStretch )
	{
		glTexCoord2f( UV_left, UV_top );
			glVertex2f( -halfWidth, -halfHeight );
		glTexCoord2f( UV_right, UV_top );
			glVertex2f( halfWidth, -halfHeight );
		glTexCoord2f( UV_right, UV_bottom );
			glVertex2f( halfWidth, halfHeight );
		glTexCoord2f( UV_left, UV_bottom );
			glVertex2f( -halfWidth, halfHeight );
	}
	else
	{
		for( int tmpY = 0; tmpY < drawData.blitZone.h; tmpY += clipZone.h )
		{
			glTexCoord2f( UV_left, UV_top );
				glVertex2f(-halfWidth, (GLfloat)(-tmpY)- clipZone.h);
			glTexCoord2f( UV_right, UV_top );
				glVertex2f(halfWidth, (GLfloat)(-tmpY)- clipZone.h);
			glTexCoord2f( UV_right, UV_bottom );
				glVertex2f(halfWidth, (GLfloat)std::fmin(clipZone.h - tmpY - clipZone.h, drawData.blitZone.h));
			glTexCoord2f( UV_left, UV_bottom );
				glVertex2f(-halfWidth, (GLfloat)std::fmin(clipZone.h - tmpY- clipZone.h, drawData.blitZone.h));
		}
	}
	glEnd();

	glColor4f( 1.0f, 1.0f, 1.0f, 1.0f );

	// Bind texture to use.
	glBindTexture( GL_TEXTURE_2D, NULL );

	Window::SwitchBlendMode( BLEND_ALPHA );
}
/*
void Texture::RenderBullet( BulletDrawData _drawData )
{
	// Remove previous transforms.
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	SDL_Rect _clipRect;
	_clipRect = _drawData.clipZone;

	// Translate to rendering point.
	glTranslatef( _drawData.xPosition, _drawData.yPosition, 0.0f );

	// Rotate around translation point if the bullet's graphic should rotate.
	if( _drawData.shouldRotate )
		glRotatef( _drawData.angle + 90, 0.0f, 0.0f, 1.0f );

	if( _drawData.addBlend )
		Window::SwitchBlendMode( BLEND_ADD );
	if( _drawData.alphaBlend )
		Window::SwitchBlendMode( BLEND_ALPHA );

	// Bind texture to use.
	glBindTexture( GL_TEXTURE_2D, textureID );

	float halfWidth  = _clipRect.w / 2.0f;
	float halfHeight = _clipRect.h / 2.0f;

	float UV_left	= (float) _clipRect.x					/ textureWidth;
	float UV_right	= (float) (_clipRect.x + _clipRect.w)	/ textureWidth;
	float UV_top	= (float) _clipRect.y					/ textureHeight;
	float UV_bottom	= (float) (_clipRect.y + _clipRect.h)	/ textureHeight;

	// Render it!
	glBegin( GL_QUADS );
	glTexCoord2f( UV_left, UV_top );
	glVertex2f(	-halfWidth,	-halfHeight );
	glTexCoord2f( UV_right, UV_top );
	glVertex2f(	 halfWidth,	-halfHeight );
	glTexCoord2f( UV_right, UV_bottom );
	glVertex2f(	 halfWidth,	 halfHeight );
	glTexCoord2f( UV_left, UV_bottom );
	glVertex2f(	-halfWidth,	 halfHeight );
	glEnd();

	// Bind texture to use.
	glBindTexture( GL_TEXTURE_2D, NULL );

	Window::SwitchBlendMode( BLEND_ALPHA );
}
void Texture::RenderLaser( LaserDrawData _drawData )
{
	// Remove previous transforms.
	glLoadIdentity();

	// Bind texture to use.
	glBindTexture( GL_TEXTURE_2D, textureID );

	SDL_Rect _clipRect;
	_clipRect = _drawData.clipZone;

	// Translate to rendering point.
	glTranslatef((GLfloat)_drawData.xStart, (GLfloat)_drawData.yStart, 0.0f);

	// Rotate laser graphic around translation point.
	glRotatef( _drawData.angle - 90, 0.0f, 0.0f, 1.0f );

	if( _drawData.addBlend )
		Window::SwitchBlendMode( BLEND_ADD );
	if( _drawData.alphaBlend )
		Window::SwitchBlendMode( BLEND_ALPHA );

	// Bind texture to use.
	glBindTexture( GL_TEXTURE_2D, textureID );

	float halfWidth  = _drawData.width * _drawData.widthScale / 2.0f;

	float UV_left	= (float) _clipRect.x					/ textureWidth;
	float UV_right	= (float) (_clipRect.x + _clipRect.w)	/ textureWidth;
	float UV_top	= (float) _clipRect.y					/ textureHeight;
	float UV_bottom	= (float) (_clipRect.y + _clipRect.h)	/ textureHeight;

	// Render lasers. Blit the graphic in a tiling manner all along its length!
	glBegin( GL_QUADS );

	for( int tmpY = 0; tmpY < _drawData.length; tmpY += _clipRect.h )
	{
		glTexCoord2f( UV_left, UV_top );
		glVertex2f(-halfWidth, (GLfloat)tmpY);
		glTexCoord2f( UV_right, UV_top );
		glVertex2f(halfWidth, (GLfloat)tmpY);
		glTexCoord2f( UV_right, UV_bottom );
		glVertex2f(halfWidth, (GLfloat)std::fmin(tmpY + _clipRect.h, _drawData.length));
		glTexCoord2f( UV_left, UV_bottom );
		glVertex2f(-halfWidth, (GLfloat)std::fmin(tmpY + _clipRect.h, _drawData.length));
	}
	glEnd();

	// Bind texture to use.
	glBindTexture( GL_TEXTURE_2D, NULL );

	Window::SwitchBlendMode( BLEND_ALPHA );
}*/