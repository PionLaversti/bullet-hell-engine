#include "ErrorReporting.h"

#include <codecvt>
#include <locale>
#include <Windows.h>

void ErrorReporting::RaiseError( std::string errorMessage )
{
	std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;
	std::wstring message = converter.from_bytes( errorMessage );

	MessageBox( nullptr, (LPCWSTR) message.c_str(), L"SHMUP Engine Error", MB_OK );

	exit( EXIT_FAILURE );
}