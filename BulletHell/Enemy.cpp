#include "Enemy.h"
#include "LuaScript.h"
#include "RandomNumberGenerator.h"

using namespace GameObjects;
using namespace GameSystems;
using namespace LuaScripting;

int Enemy::lastEnemyID;

Enemy::Enemy(const std::string scriptName) : Actor ( scriptName )
{
	SetCollisionSize( 16, 16 );
	invincible = false;

	enemyID = lastEnemyID++;
}

void Enemy::Preprocessing( )
{
	Actor::Preprocessing( );
	script->RegisterEntity( this, "Enemy" );
}

void Enemy::Initialize( )
{
	Actor::Initialize( );

	if( maximumLife == 0 )
		maximumLife = 100;

	currentLife = maximumLife;
}
void Enemy::Update()
{
	if( !active )
		return;

	if( framesLeftInMovement != 0 )
	{
		MoveBy( xSpeed, ySpeed );

		framesLeftInMovement --;
	}

	Actor::Update( );

	if( currentLife <= 0 )
		ShutDown( );
}

void Enemy::MoveToPosition( float newX, float newY, int timeInFrames )
{
	framesLeftInMovement = timeInFrames;

	xSpeed = (newX - xPosition) / timeInFrames;
	ySpeed = (newY - yPosition) / timeInFrames;
}

void Enemy::MoveToPositionInBox( float left, float top, float right, float bottom, int timeInFrames )
{
	framesLeftInMovement = timeInFrames;

	float newX = RandomNumberGenerator::RandomNumber( left, right );
	float newY = RandomNumberGenerator::RandomNumber( top, bottom );

	xSpeed = (newX - xPosition) / timeInFrames;
	ySpeed = (newY - yPosition) / timeInFrames;
}

void Enemy::Damage( const float damage )
{
	currentLife -= damage;
}