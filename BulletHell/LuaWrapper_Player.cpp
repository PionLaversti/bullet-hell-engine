#include "LuaWrapper_Player.h"
#include "LuaLibraries.h"

#include "Player.h"

using namespace GameObjects;
using namespace LuaScripting;

void LuaWrapper_Player::ExposeFunctions( lua_State* script )
{
	luabridge::getGlobalNamespace( script )
		.beginClass<Player>( "Player" )
			.addFunction( "SetCollisionSize", &Player::SetCollisionSize )
			.addFunction( "SetCollisionRect", &Player::SetCollisionRect )
			.addProperty( "CurrentX", &Player::GetX, &Player::SetX )
			.addProperty( "CurrentY", &Player::GetY, &Player::SetY )
		.endClass( );
}