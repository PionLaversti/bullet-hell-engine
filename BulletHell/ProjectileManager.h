#pragma once

#include <vector>
#include <stack>

class Texture;

namespace GameObjects
{
	namespace Projectiles
	{
		class BulletDataContainer;

		class Bullet;
		class Laser;

		class ProgrammableBullet;
		class ProgrammableLaser;
	}
}
namespace GameSystems
{
	namespace StateMachineSystem
	{
		class State_InGame;
	}

	class ProjectileManager
	{
		public:
		ProjectileManager( );
		~ProjectileManager( );

		static void Initialize( );
		static void Update( );
		static void Render( );
		static void ShutDown( );

		static void CreateLinearBullet( const float X, const float Y, const float speed, const float angle, const std::string bulTypeAlias );
		static void CreateLinearAccelBullet( const float X, const float Y, const float speed, const float acceleration, const float maxSpeed, const float angle, const std::string bulTypeAlias );
		static void CreateAngularAccelBullet( const float X, const float Y, const float speed, const float angle, const float angleAcceleration, const std::string bulTypeAlias );
		static void CreateXYAccelBullet( const float X, const float Y, const float xSpeed, const float ySpeed, const float xAccel, const float yAccel, const float xMaxSpeed, const float yMaxSpeed, const std::string bulTypeAlias );
		static void CreateStationaryLaser( const float X, const float Y, const int length, const int width, const float angle, const int warningTime, const int lifetime, const std::string bulTypeAlias );
		static void CreateRotatingLaser(		const float X, const float Y, const int length, const int width, const float angle, const float angleAcceleration, const int warningTime, const int lifetime, const std::string bulTypeAlias );

		static void CreatePlayerBullet( const float X, const float Y, const float speed, const float angle, const float damage, const std::string bulTypeAlias );

		static GameObjects::Projectiles::ProgrammableBullet* CreateProgrammableBullet( );
		static GameObjects::Projectiles::ProgrammableLaser* CreateProgrammableLaser( );

		static void Reset( );

		private:
		static const int popBulletID( );
		static const int popLaserID( );
		static const int popPlayerBulletID( );

		static std::vector<GameObjects::Projectiles::Bullet *> enemyBulletList;
		static std::vector<GameObjects::Projectiles::Laser *> enemyLaserList;
		static std::vector<GameObjects::Projectiles::Bullet *> playerBulletList;
		static std::stack<int> freeBulletIDs;
		static std::stack<int> freeLaserIDs;
		static std::stack<int> freePlayerBulletIDs;

		static bool deleteBulletsOutOfBounds;

		friend class GameSystems::StateMachineSystem::State_InGame;
	};
}