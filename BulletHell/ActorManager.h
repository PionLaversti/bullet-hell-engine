#pragma once

#include <list>

namespace GameObjects
{
	class Actor;
	class Enemy;
	class Player;
}

namespace GameSystems
{
	namespace StateMachineSystem
	{
		class State_InGame;
	}

	class ActorManager
	{
		public:
		ActorManager( );

		static void SpawnEnemy( const std::string scriptFileName );
		static void SpawnEnemyAt( const std::string scriptFileName, const int X, const int Y );
		static void SpawnPlayer( const std::string scriptFileName );

		static void Initialize( );
		static void Update( );
		static void Render( );
		static void ShutDown( );

		static GameObjects::Enemy* GetEnemyByID( const int ID );
		static GameObjects::Player* GetActivePlayer( ) {
			return activePlayer;
		}

		static bool HasZeroEnemiesOnScreen( );
		static void DestroyAllEnemiesOnScreen( );

		private:
		static GameObjects::Player* activePlayer;
		static std::list< GameObjects::Enemy* > enemies;
		static std::list< GameObjects::Enemy* >::iterator enemyIter;

		friend class ::GameSystems::StateMachineSystem::State_InGame;
	};
}