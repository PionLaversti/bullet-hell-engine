#include <SDL_rect.h>

#include "Constants.h"

#include "ProjectileManager.h"
#include "ProjectileDataManager.h"

#include "ProjectileInclude.h"

#include "DrawData.h"

#include "Bullet.h"
#include "Bullet_Player.h"
#include "Bullet_Programmable.h"
#include "Laser.h"
#include "Laser_Programmable.h"

#include "LuaInclude.h"

#include "Texture.h"

using namespace GameObjects::Projectiles;
using namespace GameSystems;
using namespace std;

std::vector<Bullet *> ProjectileManager::enemyBulletList;
std::vector<Laser *> ProjectileManager::enemyLaserList;
std::vector<Bullet *> ProjectileManager::playerBulletList;
std::stack<int> ProjectileManager::freeBulletIDs;
std::stack<int> ProjectileManager::freeLaserIDs;
std::stack<int> ProjectileManager::freePlayerBulletIDs;

bool ProjectileManager::deleteBulletsOutOfBounds;


ProjectileManager::ProjectileManager()
{
}
ProjectileManager::~ProjectileManager()
{
	for (UINT32 i = 0; i < enemyBulletList.size(); i++)
	{
		if( enemyBulletList[ i ] != NULL )
			delete enemyBulletList[ i ];
	}
}

void ProjectileManager::Initialize()
{
	enemyBulletList.resize(10000);

	for (int i = 9999; i >= 0; i--)
		freeBulletIDs.push(i);

	enemyLaserList.resize(1000);

	for (int i = 999; i >= 0; i--)
		freeLaserIDs.push(i);

	playerBulletList.resize(1000);

	for (int i = 999; i >= 0; i--)
		freePlayerBulletIDs.push(i);

	deleteBulletsOutOfBounds = true;
}
void ProjectileManager::Update( )
{
	float X;
	float Y;

	for (UINT32 i = 0; i < enemyBulletList.size(); i++)
	{
		if (enemyBulletList[i] != NULL && enemyBulletList[i]->IsActive())
		{
			enemyBulletList[i]->Update();

			if (deleteBulletsOutOfBounds)
			{
				X = enemyBulletList[i]->GetX();
				Y = enemyBulletList[i]->GetY();

				if (X < -100 || Y < -100)
					enemyBulletList[i]->ShutDown();
				if (X > _DEFAULT_SCREEN_WIDTH + 100 || Y > _DEFAULT_SCREEN_HEIGHT + 100)
					enemyBulletList[i]->ShutDown();
			}
		}
	}

	for (UINT32 i = 0; i < enemyLaserList.size(); i++)
	{
		if( enemyLaserList[ i ] != NULL && enemyLaserList[ i ]->IsActive() )
			enemyLaserList[ i ]->Update();
	}

	for (UINT32 i = 0; i < playerBulletList.size(); i++)
	{
		if( playerBulletList[i] != NULL && playerBulletList[i]->IsActive( ) )
		{
			playerBulletList[i]->Update( );

			X = playerBulletList[i]->GetX();
			Y = playerBulletList[i]->GetY();

			if (X < -100 || Y < -100)
				playerBulletList[i]->ShutDown();
			if (X > _DEFAULT_SCREEN_WIDTH + 100 || Y > _DEFAULT_SCREEN_HEIGHT + 100)
				playerBulletList[i]->ShutDown();
		}

	}
}
void ProjectileManager::Render( )
{
	Texture* bulletTexture = ProjectileDataManager::GetProjectileTexture( "_standard" );

	for( UINT32 i = 0; i < enemyLaserList.size(); i ++ )
	{
		if( enemyLaserList[ i ] != NULL && enemyLaserList[ i ]->IsActive() )
			bulletTexture->Render( enemyLaserList[ i ] ->GetDrawData() );
	}

	for (UINT32 i = 0; i < enemyBulletList.size(); i++)
	{
		if( enemyBulletList[ i ] != NULL && enemyBulletList[ i ]->IsActive() )
			bulletTexture->Render( enemyBulletList[ i ]->GetDrawData() );
	}

	for (UINT32 i = 0; i < playerBulletList.size(); i++)
	{
		if( playerBulletList[ i ] != NULL && playerBulletList[ i ]->IsActive() )
			bulletTexture->Render( playerBulletList[ i ]->GetDrawData() );
	}

	bulletTexture = nullptr;
}

void ProjectileManager::ShutDown( )
{
	for( UINT32 i = 0; i < enemyLaserList.size(); i ++ )
	{
		if( enemyLaserList[i] != NULL && enemyLaserList[i]->IsActive( ) )
			enemyLaserList[i]->ShutDown( );
	}

	for (UINT32 i = 0; i < enemyBulletList.size(); i++)
	{
		if( enemyBulletList[i] != NULL && enemyBulletList[i]->IsActive( ) )
			enemyBulletList[i]->ShutDown( );
	}

	for (UINT32 i = 0; i < playerBulletList.size(); i++)
	{
		if( playerBulletList[i] != NULL && playerBulletList[i]->IsActive( ) )
			playerBulletList[i]->ShutDown( );
	}
}

void ProjectileManager::CreateLinearBullet(			const float X, const float Y, const float speed, const float angle, const string bulTypeAlias )
{
	ProjectileType* type = ProjectileDataManager::FindByAlias( bulTypeAlias );

	int tmpID = popBulletID( );

	enemyBulletList[tmpID] = new Bullet_Linear( X, Y, speed, angle, type );
}
void ProjectileManager::CreateLinearAccelBullet(	const float X, const float Y, const float speed, const float acceleration, const float maxSpeed, const float angle, const string bulTypeAlias )
{
	ProjectileType* type = ProjectileDataManager::FindByAlias( bulTypeAlias );

	int tmpID = popBulletID( );

	enemyBulletList[tmpID] = new Bullet_LinearAccel( X, Y, speed, acceleration, maxSpeed, angle, type );
}
void ProjectileManager::CreateAngularAccelBullet(	const float X, const float Y, const float speed, const float angle, const float angleAcceleration, const string bulTypeAlias )
{
	ProjectileType* type = ProjectileDataManager::FindByAlias( bulTypeAlias );

	int tmpID = popBulletID( );

	enemyBulletList[tmpID] = new Bullet_AccelAng( X, Y, speed, angle, angleAcceleration, type );
}
void ProjectileManager::CreateXYAccelBullet(		const float X, const float Y, const float xSpeed, const float ySpeed, const float xAccel, const float yAccel, const float xMaxSpeed, const float yMaxSpeed, const string bulTypeAlias )
{
	ProjectileType* type = ProjectileDataManager::FindByAlias( bulTypeAlias );

	int tmpID = popBulletID( );

	enemyBulletList[tmpID] = new Bullet_AccelXY( X, Y, xSpeed, ySpeed, xAccel, yAccel, xMaxSpeed, yMaxSpeed, type );
}
void ProjectileManager::CreateStationaryLaser(		const float X, const float Y, const int length, const int width, const float angle, const int warningTime, const int lifetime, const string bulTypeAlias )
{
	ProjectileType* type = ProjectileDataManager::FindByAlias( bulTypeAlias );

	int tmpID = popLaserID( );

	enemyLaserList[ tmpID ] = new Laser_Stationary( X, Y, angle, length, width, warningTime, lifetime, type );
}

void ProjectileManager::CreateRotatingLaser(		const float X, const float Y, const int length, const int width, const float angle, const float angleAcceleration, const int warningTime, const int lifetime, const string bulTypeAlias )
{
	ProjectileType* type = ProjectileDataManager::FindByAlias( bulTypeAlias );

	int tmpID = popLaserID( );

	enemyLaserList[ tmpID ] = new Laser_Rotating( X, Y, angle, angleAcceleration, length, width, warningTime, lifetime, type );
}

void ProjectileManager::CreatePlayerBullet(			const float X, const float Y, const float speed, const float angle, const float damage, const string bulTypeAlias )
{
	ProjectileType* type = ProjectileDataManager::FindByAlias( bulTypeAlias );

	int tmpID = popPlayerBulletID( );

	playerBulletList[ tmpID ] = new Bullet_Player( X, Y, speed, angle, damage, type );
}

const int ProjectileManager::popBulletID( )
{
	int ID = freeBulletIDs.top( );
	freeBulletIDs.pop( );

	return ID;
}
const int ProjectileManager::popLaserID( )
{
	int ID = freeLaserIDs.top( );
	freeLaserIDs.pop( );

	return ID;
}
const int ProjectileManager::popPlayerBulletID( )
{
	int ID = freePlayerBulletIDs.top( );
	freePlayerBulletIDs.pop( );

	return ID;
}

ProgrammableBullet* ProjectileManager::CreateProgrammableBullet( )
{
	int tmpID = popBulletID( );

	ProgrammableBullet* bullet = new ProgrammableBullet( );

	enemyBulletList[tmpID] = bullet;

	return bullet;
}

ProgrammableLaser* ProjectileManager::CreateProgrammableLaser( )
{
	int tmpID = popLaserID( );

	ProgrammableLaser* laser = new ProgrammableLaser( );

	enemyLaserList[tmpID] = laser;

	return laser;
}

void ProjectileManager::Reset( )
{
	for( UINT32 i = 0; i < enemyBulletList.size( ); i++ )
	{
		if( enemyBulletList[i] != NULL )
			enemyBulletList[i]->ShutDown( );
	}

	for( UINT32 i = 0; i < enemyLaserList.size( ); i++ )
	{
		if( enemyLaserList[i] != NULL )
			enemyLaserList[i]->ShutDown( );
	}

	for( UINT32 i = 0; i < playerBulletList.size( ); i++ )
	{
		if( playerBulletList[i] != NULL )
			playerBulletList[i]->ShutDown( );
	}

	while( !freeBulletIDs.empty( ) )
		freeBulletIDs.pop( );

	for( int i = 9999; i >= 0; i-- )
		freeBulletIDs.push( i );

	while( !freeLaserIDs.empty( ) )
		freeLaserIDs.pop( );

	for( int i = 999; i >= 0; i-- )
		freeLaserIDs.push( i );

	while( !freePlayerBulletIDs.empty( ) )
		freePlayerBulletIDs.pop( );

	for( int i = 999; i >= 0; i-- )
		freePlayerBulletIDs.push( i );
}