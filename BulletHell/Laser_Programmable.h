#pragma once

#include <string>
#include "Laser.h"

namespace GameObjects
{
	namespace Projectiles
	{
		class ProgrammableLaser : public Laser
		{
			public:
			ProgrammableLaser( );

			void Update( );

			void SetGraphic( const std::string bulTypeAlias );
			void SetAngle( const float newAngle ) { angle = newAngle; }
			void SetLength( const int newLength ) { length = newLength; }
			void SetWidth( const int newWidth ) { width = newWidth; }
			void SetX( const float newX ) { startX = newX; }
			void SetY( const float newY ) { startY = newY; }
			void SetLifetime( const int newLifetime )  { lifetime = newLifetime; }
			void SetWarningTime( const int newWarningTime ) { warningTime = newWarningTime; }

			void SetXSpeed( const float newXSpeed ) { xSpeed = newXSpeed; }
			void SetYSpeed( const float newYSpeed ) { ySpeed = newYSpeed; }
			void SetXAccel( const float newXAccel ) { xAccel = newXAccel; }
			void SetYAccel( const float newYAccel ) { yAccel = newYAccel; }
			void SetXMaxSpeed( const float newXMaxSpeed );
			void SetYMaxSpeed( const float newYMaxSpeed );
			void SetAngAccel( const float newAngAccel ) { angAccel = newAngAccel; }

			void SetXYSpeed( const float xSpeed, const float ySpeed );
			void SetXYAccel( const float xAccel, const float yAccel );
			void SetXYMaxSpeed( const float xMaxSpeed, const float yMaxSpeed );

			std::string GetGraphic( ) const { return laserTypeAlias; }
			float GetXSpeed( ) const { return xSpeed; }
			float GetYSpeed( ) const { return ySpeed; }
			float GetXAccel( ) const { return xAccel; }
			float GetYAccel( ) const { return yAccel; }
			float GetXMaxSpeed( ) const { return xMaxSpeed; }
			float GetYMaxSpeed( ) const { return yMaxSpeed; }
			float GetAngAccel( ) const { return angAccel; }
			int GetLifetime( ) const { return lifetime; }
			int GetWarningTime( ) const { return warningTime; }

			private:
			std::string laserTypeAlias;
			float xSpeed;
			float ySpeed;
			float xAccel;
			float yAccel;
			float xMaxSpeed;
			float yMaxSpeed;
			float angAccel;

			bool xMinimumSpeedLimit;
			bool xMaximumSpeedLimit;
			bool yMinimumSpeedLimit;
			bool yMaximumSpeedLimit;
		};
	}
}