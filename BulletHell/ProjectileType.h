#pragma once

#include <SDL_rect.h>
#include <string>

namespace GameObjects
{
	namespace Projectiles
	{
		struct ProjectileType
		{
			bool shouldRotate = false;
			int collisionHeight = 0;
			int collisionWidth = 0;

			float rotateRate = 0;
			
			SDL_Rect graphicRect = { 0, 0, 0, 0 };

			bool addBlend = false;
			bool alphaBlend = false;
		};
	}
}