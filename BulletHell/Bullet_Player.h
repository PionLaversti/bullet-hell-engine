#pragma once

#include "Bullet.h"

namespace GameObjects
{
	namespace Projectiles
	{
		class Bullet_Player : public Bullet
		{
			public:
			Bullet_Player( const float xStart, const float yStart, const float speed, const float angle, const float damage, ProjectileType* bulType );

			void Update( );
			void ShutDown( );

			const float GetDamage( ) { return damage; }

			private:
			float damage;
			static GameSystems::CollisionGrid* playerBulletGrid;

			friend class ::GameSystems::CollisionDetector;
		};
	}
}