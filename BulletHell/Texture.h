#pragma once

#include <assert.h>
#include <string>
#include <vector>
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_opengl.h>

namespace GameObjects
{
	struct DrawData;
}

class Texture
{
public:
	Texture();
	~Texture();

	bool LoadTextureFromFile( std::string path);
	void FreeTexture();
	void Render( GLfloat x, GLfloat y );
	void RenderToSize( GLfloat x, GLfloat y, GLfloat width, GLfloat height );
	void RenderInRect( GLfloat left, GLfloat top, GLfloat right, GLfloat bottom );

	void Render( GameObjects::DrawData drawData );

//	void RenderBullet( GameObjects::Projectiles::BulletDrawData drawData );
//	void RenderLaser( GameObjects::Projectiles::LaserDrawData drawData );
	const GLuint GetTextureID()		{ return textureID; }
	const GLuint GetTextureWidth()	{ return textureWidth; }
	const GLuint GetTextureHeight()	{ return textureHeight; }

private:
	GLuint textureID;
	int textureWidth;
	int textureHeight;
};