#pragma once

#include <assert.h>
#include <string>
#include <SDL.h>
#include <SDL_opengl.h>

#include "GLWrapper.h"

class Window
{
public:
	Window();

	bool Initialize();
	bool InitOpenGL();
	static void SwitchBlendMode(GL_Blend_Mode blendMode);

	static void PrintDebugText( int X, int Y, std::string text );
	void Render( int X, int Y, SDL_Surface*, SDL_Rect* );
	void ClearWindow( );
	void UpdateWindow();
	void SetWindowCaption( std::string, std::string );

private:
	SDL_Window* window;
	static SDL_Surface* screen;
	SDL_GLContext glContext;
};