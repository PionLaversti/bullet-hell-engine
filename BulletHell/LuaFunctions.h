#pragma once

#include <iostream>
#include <string>
#include <Windows.h>

#include "LuaInclude.h"

namespace GameObjects
{
	class Enemy;
}

namespace LuaScripting
{
	class LuaFunctions
	{
		public:
		static lua_State* OpenScript( std::string file );
		static void CloseScript( lua_State* script );
		static void LogError( lua_State* state );

		static void RegisterFunction( lua_State* script, std::string functionName, lua_CFunction function);
		static void RegisterFunction( lua_State* script, std::string tableName, std::string functionName, lua_CFunction function);
		static void CallFunction( lua_State* script, std::string functionName );
		static std::string GetScriptName( lua_State* script );
		static std::string GetScriptNameAndLineNumber( lua_State* script );
		static std::string GetArgumentType( lua_State* script, const int argNumber );
		
		static void MatchArguments( lua_State* script, std::string functionName, int numArgs ... );

		private:
		static void RegisterScriptFileName( lua_State* state, const std::string fileName );
		static void ExposeLibraries( lua_State* state );
	};
}