#include "Bullet_LinearAccel.h"

using namespace GameObjects::Projectiles;

Bullet_LinearAccel::Bullet_LinearAccel( ) : Bullet( ) { }

Bullet_LinearAccel::Bullet_LinearAccel( const float X, const float Y, const float spd, const float accel, const float maxSpd, 
										const float ang, ProjectileType* bulType )
	: Bullet( X, Y, spd, ang, bulType )
{
	acceleration	= accel;
	maxSpeed		= maxSpd;
}

void Bullet_LinearAccel::Update()
{
	Bullet::Update( );

	if( deathFramesLeft == -1 )
	{
		if( acceleration < 0 )
		{
			if( speed > maxSpeed )
				speed += acceleration;
		}
		else if( acceleration > 0 )
		{
			if( speed < maxSpeed )
				speed += acceleration;
		}

		xSpeed = speed * (float) cos( angle );
		ySpeed = speed * (float) sin( angle );
	}
}