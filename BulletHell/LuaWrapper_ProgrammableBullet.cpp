#include "Bullet_Programmable.h"
#include "LuaWrapper_ProgrammableBullet.h"

using namespace GameObjects::Projectiles;
using namespace LuaScripting;

void LuaWrapper_ProgrammableBullet::ExposeFunctions( lua_State* script )
{
	luabridge::getGlobalNamespace( script )
		.beginClass<ProgrammableBullet>( "ProgrammableBullet" )
			.addProperty( "Graphic", &ProgrammableBullet::GetGraphic, &ProgrammableBullet::SetGraphic )
			.addProperty( "CurrentX", &ProgrammableBullet::GetX, &ProgrammableBullet::SetX )
			.addProperty( "CurrentY", &ProgrammableBullet::GetY, &ProgrammableBullet::SetY )
			.addProperty( "Speed", &ProgrammableBullet::GetSpeed, &ProgrammableBullet::SetSpeed )
			.addProperty( "Acceleration", &ProgrammableBullet::GetAcceleration, &ProgrammableBullet::SetAcceleration )
			.addProperty( "MaxSpeed" , &ProgrammableBullet::GetMaxSpeed, &ProgrammableBullet::SetMaxSpeed )
			.addProperty( "Angle", &ProgrammableBullet::GetAngle, &ProgrammableBullet::SetAngle )
			.addProperty( "AngleAcceleration", &ProgrammableBullet::GetAngleAcceleration, &ProgrammableBullet::SetAngleAcceleration )
			.addProperty( "XSpeed", &ProgrammableBullet::GetXSpeed, &ProgrammableBullet::SetXSpeed )
			.addProperty( "YSpeed", &ProgrammableBullet::GetYSpeed, &ProgrammableBullet::SetYSpeed )
			.addProperty( "XAcceleration", &ProgrammableBullet::GetYAccel, &ProgrammableBullet::SetXAccel )
			.addProperty( "YAcceleration", &ProgrammableBullet::GetYAccel, &ProgrammableBullet::SetYAccel )
			.addProperty( "XMaxSpeed", &ProgrammableBullet::GetXMaxSpeed, &ProgrammableBullet::SetXMaxSpeed )
			.addProperty( "YMaxSpeed", &ProgrammableBullet::GetYMaxSpeed, &ProgrammableBullet::SetYMaxSpeed )
			.addFunction( "SetXYSpeed", &ProgrammableBullet::SetXYSpeed )
			.addFunction( "SetXYAccel", &ProgrammableBullet::SetXYAccel )
			.addFunction( "SetXYMaxSpeed", &ProgrammableBullet::SetXYMaxSpeed )
		.endClass( );
}