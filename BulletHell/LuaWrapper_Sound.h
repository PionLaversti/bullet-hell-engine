#pragma once

#include <string>

#include "LuaClassWrapper.h"

namespace LuaScripting
{
	class LuaWrapper_Sound : public LuaClassWrapper
	{
		public:
		static void ExposeFunctions( lua_State* script );

		private:
		static void PlayMusic( const std::string fileName );
		static void PlaySE( const std::string fileName );
	};
}