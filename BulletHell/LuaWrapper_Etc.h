#pragma once

#include "LuaClassWrapper.h"

namespace LuaScripting
{
	class LuaWrapper_Etc : LuaClassWrapper
	{
		public:
		static void ExposeFunctions( lua_State* script );

	};
}