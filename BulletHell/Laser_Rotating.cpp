#include "Laser_Rotating.h"

using namespace GameObjects::Projectiles;

Laser_Rotating::Laser_Rotating()
{
}

Laser_Rotating::Laser_Rotating( const float newX, const float newY, const float newAngle, const float newAngAccel,
								const int newLength, const int newWidth, const int newWarningTime, const int newLifetime, ProjectileType* const type )
	: Laser( newX, newY, newAngle, newLength, newWidth, newWarningTime, newLifetime, type )
{
	angAccel = newAngAccel;
}

Laser_Rotating::~Laser_Rotating()
{
}

void Laser_Rotating::Update( )
{
	Laser::Update( );

	angle += angAccel;
}