#include "LuaWrapper_Enemy.h"

#include "ActorManager.h"
#include "Enemy.h"
#include "Lifebar.h"
#include "LuaLibraries.h"

using namespace GameObjects;
using namespace GameSystems;
using namespace LuaScripting;

void LuaWrapper_Enemy::ExposeFunctions( lua_State* script )
{
	luabridge::getGlobalNamespace( script )
		.addFunction( "CreateLifebar", &LuaWrapper_Enemy::CreateLifebar )
		.beginClass<Enemy>( "Enemy" )
			.addFunction( "GetID", &Enemy::GetID )
			.addProperty( "CurrentX", &Enemy::GetX, &Enemy::SetX )
			.addProperty( "CurrentY", &Enemy::GetY, &Enemy::SetY )
			.addProperty( "CurrentLife", &Enemy::GetLife, &Enemy::SetLife )
			.addProperty( "MaximumLife", &Enemy::GetMaxLife, &Enemy::SetMaxLife )
			.addFunction( "MoveToPosition", &Enemy::MoveToPosition )
			.addFunction( "MoveToPositionInBox", &Enemy::MoveToPositionInBox )
//			.addFunction( "SetCollisionSize", &GameObject::SetCollisionSize )
			.addFunction( "SetCollisionRect", &Enemy::SetCollisionRect )
			.addFunction( "Damage", &Enemy::Damage )
		.endClass( );
}

void LuaWrapper_Enemy::CreateLifebar( const std::string fileName, const int ID )
{
	Lifebar* lifebar = new Lifebar( fileName );

	Enemy* enemy = ActorManager::GetEnemyByID( ID );

	lifebar->LinkEnemy( enemy );

	enemy = nullptr;
}