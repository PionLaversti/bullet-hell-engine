#include "ProjectileDataManager.h"

#include <boost/algorithm/string.hpp>
#include "LuaFunctions.h"
#include "ProjectileDataFile.h"

using namespace GameObjects::Projectiles;
using namespace GameSystems;
using namespace LuaScripting;
using namespace std;

map< string, ProjectileDataFile* > ProjectileDataManager::loadedFiles;

void ProjectileDataManager::LoadDataFile( const string fileName )
{
	lua_State* luaFile = LuaFunctions::OpenScript( fileName );

	ProjectileDataFile* projectileLibrary = new ProjectileDataFile( );

	projectileLibrary->LoadDataFile( luaFile );

	if( loadedFiles[projectileLibrary->GetLibraryName( )] == nullptr )
		loadedFiles[projectileLibrary->GetLibraryName( )] = projectileLibrary;
}

ProjectileType* ProjectileDataManager::FindByAlias( const string alias )
{
	vector<string> tokens;

	boost::split( tokens, alias, boost::is_any_of( ":" ), boost::algorithm::token_compress_on );

	if( tokens.size( ) == 1 )
		return FindLibraryByAlias( "_standard" )->FindByAlias( alias );

	return FindLibraryByAlias( tokens[0] )->FindByAlias( tokens[1] );
}

ProjectileDataFile* ProjectileDataManager::FindLibraryByAlias( const string alias )
{
	ProjectileDataFile* data = loadedFiles[alias];

	if( data == nullptr )
		throw invalid_argument( "Could not find projectile library \"" + alias + "\"" );

	return data;
}

Texture* ProjectileDataManager::GetProjectileTexture( const string libraryName )
{
	return FindLibraryByAlias( "_standard" )->GetBulletTexture( );
}