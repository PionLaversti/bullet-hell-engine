#pragma once

#include <SDL_rect.h>

#include "GLWrapper.h"

namespace GameObjects
{
	struct DrawData
	{
		SDL_Rect blitZone;
		SDL_Rect clipZone;
		GL_Blend_Mode blendMode;

		bool shouldStretch;
		bool shouldRotate;
		float alpha;
		float angle;
	};
}