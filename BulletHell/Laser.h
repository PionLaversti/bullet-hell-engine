#pragma once

namespace GameObjects
{
	struct DrawData;

	namespace Projectiles
	{
		struct ProjectileType;
		struct LaserDrawData;

		class Laser
		{
			public:
			Laser( );
			Laser( const float newX, const float newY, const float newAngle, const int newLength, const int newWidth, const int warningTime, const int newLifetime, ProjectileType* const laserType );

			virtual void ShutDown( );
			virtual void Update( );
			virtual const DrawData GetDrawData( );

			virtual bool IsActive( ) const { return isActive; };
			virtual bool IsHitboxActive( ) const { return warningTime <= 0; };
			virtual float GetStartX( ) const { return startX; }
			virtual float GetStartY( ) const { return startY; }
			virtual float GetAngle( ) const { return angle; }
			virtual int GetLength( ) const { return length; }
			virtual int GetWidth( ) const { return width; }

			protected:
			ProjectileType*	laserType;
			float		startX;
			float		startY;
			float		angle;
			int			length;
			int			width;
			int			lifetime;
			int			warningTime;
			bool		isActive;
		};
	}
}