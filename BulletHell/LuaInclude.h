#pragma once

extern "C"
{
	#include <lua.h>
	#include <luaconf.h>
	#include <lualib.h>
	#include <lauxlib.h>
}

#include <LuaBridge.h>