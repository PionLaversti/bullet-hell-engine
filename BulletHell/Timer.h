#pragma once

#include <assert.h>
#include "SDL.h"

class Timer
{
public:
	Timer();

	void Pause();
	void Unpause();
	void Start();
	void Stop();
	void Reset();

	Uint32 GetElapsedTime();

	bool IsPaused();
	bool IsStarted();
private:
		// Clock time the timer was started.
	Uint32 startAt;
		// Number of ticks passed when the timer was paused.
	Uint32 pausedAt;

	bool paused;
	bool started;
};