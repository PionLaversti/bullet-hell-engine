#include "ImageWrapper.h"

void apply_surface( const int _x, const int _y, SDL_Surface* _src, SDL_Surface* _dst, SDL_Rect* _clip = NULL )
{
		// Create a temporary rectangle to hold offsets
	SDL_Rect offset;

		// Pass args to rectangle
	offset.x = _x;
	offset.y = _y;

	SDL_BlitSurface( _src, _clip, _dst, &offset );
}

bool write_message( TTF_Font* _font, SDL_Surface** _target, const std::string _msg, const SDL_Color _col )
{
	*_target = TTF_RenderText_Solid( _font, _msg.c_str(), _col );

	if( !_target )
		return false;
	
	return true;
}