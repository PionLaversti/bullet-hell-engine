#include "Laser_Stationary.h"

using namespace GameObjects::Projectiles;

Laser_Stationary::Laser_Stationary()
{
}
Laser_Stationary::Laser_Stationary( const float newX, const float newY, const float newAngle, const int newLength, const int newWidth,
									const int newWarningTime, const int newLifetime, ProjectileType* const type )
	: Laser( newX, newY, newAngle, newLength, newWidth, newWarningTime, newLifetime, type )
{
}
Laser_Stationary::~Laser_Stationary()
{
}