#pragma once

#pragma warning (disable : 4005)

#include <SDL.h>
#include <SDL_Mixer.h>
#include <SDL_TTF.h>

namespace GameSystems
{
	namespace StateMachineSystem
	{
		class StateMachine;
	}
}

class Window;

class Application
{
public:
		// Constructor. Blank.
	Application();
	
		// Initializes subsystems.
	bool Initialize();
		// Runs the program and maintains the main loop.
	int run();

	Window* window;
	static float GetFPS( ) { return fps; }

	private:

		// Closes the program and all subsystems.
	void close();
	SDL_Event event;

	bool firstFrame = true;
	int frames = 0;
	double elapsed = 0;
	double lastUpdateTime = 0;
	static float fps;
};