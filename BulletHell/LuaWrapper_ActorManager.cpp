#include "LuaWrapper_ActorManager.h"

#include <math.h>

#include "ActorManager.h"
#include "Player.h"

using namespace GameObjects;
using namespace GameSystems;
using namespace LuaScripting;

void LuaWrapper_ActorManager::ExposeFunctions( lua_State* script )
{
	luabridge::getGlobalNamespace( script )
		.addFunction( "SpawnEnemy", &ActorManager::SpawnEnemy )
		.addFunction( "SpawnEnemyAt", &ActorManager::SpawnEnemyAt )
		.addFunction( "GetPlayerX", &LuaWrapper_ActorManager::GetPlayerX )
		.addFunction( "GetPlayerY", &LuaWrapper_ActorManager::GetPlayerY )
		.addFunction( "GetAngleToPlayer", &LuaWrapper_ActorManager::GetAngleToPlayer )
		.addFunction( "HasNoEnemiesOnScreen", &ActorManager::HasZeroEnemiesOnScreen )
		.addFunction( "DestroyAllEnemiesOnScreen", &ActorManager::DestroyAllEnemiesOnScreen );
}

float LuaWrapper_ActorManager::GetPlayerX( )
{
	return ActorManager::GetActivePlayer( )->GetX( );
}
float LuaWrapper_ActorManager::GetPlayerY( )
{
	return ActorManager::GetActivePlayer( )->GetY( );
}
float LuaWrapper_ActorManager::GetAngleToPlayer( const float otherX, const float otherY )
{
	float playerX = ActorManager::GetActivePlayer( )->GetX( );
	float playerY = ActorManager::GetActivePlayer( )->GetY( );

	return (float) (atan2( playerY - otherY, playerX - otherX ) * (180 / M_PI));
}