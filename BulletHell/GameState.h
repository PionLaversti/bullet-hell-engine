///	--	--	--	--	--	--	--	--	--	--	--	--	--	--	--	--	--	--	--	--
/// GameState.h
/// Abstract base class for all game states.
///	Provides virtual definitions for all gamestates' functions to use and make my job easier.
///	--	--	--	--	--	--	--	--	--	--	--	--	--	--	--	--	--	--	--	--

#pragma once

namespace GameSystems
{
	namespace StateMachineSystem
	{
		enum GameStateType : int
		{
			STATE_NULL = 0,
			STATE_INIT,
			STATE_TITLE,
			STATE_MENU,
			STATE_INGAME,
			STATE_EXIT
		};

		class GameState
		{
			public:
			virtual GameStateType GetNextState() = 0;
			virtual void HandleEvents() = 0;
			virtual void HandleLogic() = 0;
			virtual void Render() = 0;
			virtual ~GameState() { };

			protected:
			GameStateType nextState;
		};
	}
}