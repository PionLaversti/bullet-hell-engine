#pragma once

#include <map>
#include <string>

class Texture;

namespace GameObjects
{
	namespace Projectiles
	{
		struct ProjectileType;
	}
}

namespace GameSystems
{
	class ProjectileDataFile;

	class ProjectileDataManager
	{
		public:
		static void LoadDataFile( const std::string fileName );
		static GameObjects::Projectiles::ProjectileType* FindByAlias( const std::string alias );
		static Texture* GetProjectileTexture( const std::string libraryName );

		private:
		static ProjectileDataFile* FindLibraryByAlias( const std::string alias );

		static std::map<std::string, ProjectileDataFile*> loadedFiles;
	};
}