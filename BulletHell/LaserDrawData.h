#pragma once

#include <SDL_rect.h>

namespace GameObjects
{
	namespace Projectiles
	{
		struct LaserDrawData
		{
			int xStart;
			int yStart;
			int length;
			int width;
			float widthScale;
			float angle;
			bool alphaBlend;
			bool addBlend;
			SDL_Rect clipZone;
		};
	}
}