#include "RandomNumberGenerator.h"

using namespace GameSystems;

boost::random::mt19937 RandomNumberGenerator::RNG;

float RandomNumberGenerator::RandomNumber( const float lowBound, const float highBound )
{
	boost::random::uniform_real_distribution<> random( lowBound, highBound );
	return random( RNG );
}
void RandomNumberGenerator::SeedRNG( const int seed )
{
	RNG.seed( seed );
}