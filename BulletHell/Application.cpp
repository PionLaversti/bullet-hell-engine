#pragma once

#include "Application.h"
#include "LuaWrapper.h"
#include "ResourceManager.h"
#include "SoundWrapper.h"

#include "Constants.h"
#include "GLWrapper.h"
#include "StateMachine.h"
#include "Timer.h"
#include "Window.h"

using namespace GameSystems::StateMachineSystem;

float Application::fps;

Application::Application()
{
}

bool Application::Initialize()
{
		// Start SDL.
	if( SDL_Init( SDL_INIT_EVERYTHING ) == -1 )
		return false;

	window = new Window( );

	if( !window->Initialize() )
		return false;

	if( TTF_Init() == -1 )
		return false;

	if( Mix_OpenAudio( 22050, MIX_DEFAULT_FORMAT, 2, 512 ) == -1 )
		return false;

	SoundWrapper::Initialize();

	if(LuaScripting::LuaWrapper::Initialize( ) == false)
		return false;

	ResourceManager::LoadFont( "lucon.ttf", 16 );

	StateMachine::Initialize();

	return true;
}
int Application::run()
{
	Timer frameDelayTimer;

	//	--	--	--	--	--	--	--	--	--	--	--	--	--	--	--
	// START GAME LOOP
	//	--	--	--	--	--	--	--	--	--	--	--	--	--	--	--
	while( !StateMachine::HasStopped() )
	{
			// Start the frame timer.
		frameDelayTimer.Start();

			// GAMESTATE: Handle events.
		StateMachine::HandleEvents();

			// GAMESTATE: Handle logic.
		StateMachine::Update();

			// Change GAMESTATE if needed.
		StateMachine::ChangeState();

			// Clear the window->
		glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

			// GAMESTATE: Render.
		StateMachine::Render();

		window->UpdateWindow();

		int timePassed = frameDelayTimer.GetElapsedTime();

			// Cap framerate.
		if( timePassed < (1000.0f / _FRAMES_PER_SECOND) )
		{
			frames++;
			elapsed += timePassed;

			if( elapsed - lastUpdateTime > 250.0f )
			{
				fps = frames * 1000 / (float) (elapsed - lastUpdateTime);
				lastUpdateTime = elapsed;
				frames = 0;
			}

			SDL_Delay( (Uint32) (1000.0f / _FRAMES_PER_SECOND) - timePassed );
		}
		
		frameDelayTimer.Reset();
	}
	//	--	--	--	--	--	--	--	--	--	--	--	--	--	--	--
	// END GAME LOOP
	//	--	--	--	--	--	--	--	--	--	--	--	--	--	--	--

		// Close application
	close();

	return 0;
}
void Application::close()
{
	ResourceManager::release_all();

		// Quit SDL external libraries
	Mix_CloseAudio();
	TTF_Quit();
    
		// Quit SDL
    SDL_Quit();
}