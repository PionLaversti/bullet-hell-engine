#pragma once

#include "Actor.h"

namespace GameObjects
{
	class Enemy : public Actor
	{
		public:
		Enemy( const std::string scriptFileName );

		void Preprocessing( );
		void Initialize( );
		void Update( );

		void MoveToPosition( float X, float Y, int frames );
		void MoveToPositionInBox( float left, float top, float right, float bottom, int frames );

		int GetID( ) const { return enemyID; }

		void SetLife( const float newLife ) { currentLife = newLife; }
		float GetLife( ) const { return currentLife; }
		void SetMaxLife( const float newLife ) { maximumLife = newLife; }
		float GetMaxLife( ) const { return maximumLife; }

		void Damage( const float damage );

		void SetCollisionRect( float X, float Y, float width, float height ) { GameObject::SetCollisionRect( X, Y, width, height ); }

		private:
		int enemyID;
		int framesLeftInMovement;
		float xSpeed;
		float ySpeed;

		float currentLife;
		float maximumLife;

		static int lastEnemyID;
	};
}