#pragma once

#include "Bullet.h"

namespace GameObjects
{
	namespace Projectiles
	{
		class Bullet_AccelXY : public Bullet
		{
			public:
			Bullet_AccelXY( );
			Bullet_AccelXY( const float xPosStart, const float yPosStart, const float xSpeedStart,
							const float ySpeedStart, const float xAccel, const float yAccel, const float xMaxVel, const float yMaxVel, ProjectileType* bulType );

			void Update( );

			private:
			float maxXVelocity;
			float maxYVelocity;
			float xAccel;
			float yAccel;
		};
	}
}