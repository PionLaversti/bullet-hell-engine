#include "Bullet_AccelXY.h"

using namespace GameObjects::Projectiles;

Bullet_AccelXY::Bullet_AccelXY( ) : Bullet( ) { }

Bullet_AccelXY::Bullet_AccelXY( const float X, const float Y, const float xSpd, const float ySpd, const float xAcc, 
								const float yAcc, const float xMax, const float yMax, ProjectileType* bulType )
	: Bullet( X, Y, 0, 0, bulType )
{
	xSpeed		= xSpd;
	ySpeed		= ySpd;
	maxXVelocity	= xMax;
	maxYVelocity	= yMax;
	xAccel		= xAcc;
	yAccel		= yAcc;
}

void Bullet_AccelXY::Update()
{
	Bullet::Update( );

	if( deathFramesLeft == -1 )
	{
		if( maxXVelocity > xSpeed || maxXVelocity == -1200.05f )
		{
			xSpeed += xAccel;

			if( maxXVelocity < xSpeed && maxXVelocity != -1200.05f )
				xSpeed = maxXVelocity;
		}

		if( maxYVelocity > ySpeed || maxYVelocity == -1200.05f )
		{
			ySpeed += yAccel;

			if( maxYVelocity < ySpeed && maxYVelocity != -1200.05f )
				ySpeed = maxYVelocity;
		}

		angle = (float) atan2( ySpeed, xSpeed ) / (float) (M_PI / 180);
	}
}