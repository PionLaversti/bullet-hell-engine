#include "CollisionGrid.h"

#include <algorithm>

using namespace GameObjects;
using namespace GameSystems;
using namespace std;

void CollisionGrid::Initialize( const int screenWidth, const int screenHeight )
{
	objectLists.resize( (screenWidth / GRID_SIZE_PX) + (BUFFER_SQUARES * 2) );

	for( int ind = 0; ind < objectLists.size( ); ind++ )
		objectLists[ind].resize( (screenHeight / GRID_SIZE_PX) + (BUFFER_SQUARES * 2) );
}

const int CollisionGrid::GetSquarePointLiesIn( const int val )
{
	int px = GRID_SIZE_PX;
	int buff = BUFFER_SQUARES;

	return (val / px) + buff;
}

const bool CollisionGrid::HasObjectsInOverlappedSquares( SDL_Rect target )
{
	int left = GetSquarePointLiesIn( target.x );
	int right = GetSquarePointLiesIn( target.x + target.w );
	int top = GetSquarePointLiesIn( target.y );
	int bottom = GetSquarePointLiesIn( target.y + target.h );

	return HasObjectsInGridSquare( left, top ) ||
		HasObjectsInGridSquare( right, top ) ||
		HasObjectsInGridSquare( left, bottom ) ||
		HasObjectsInGridSquare( right, bottom );
}
std::vector< GameObject* > CollisionGrid::GetObjectsInOverlappedSquares( SDL_Rect target )
{
	int left = GetSquarePointLiesIn( target.x - (target.w / 2) );
	int right = GetSquarePointLiesIn( target.x + (target.w / 2) );
	int top = GetSquarePointLiesIn( target.y - (target.h / 2) );
	int bottom = GetSquarePointLiesIn( target.y + (target.h / 2) );

	std::vector< GameObject* > objects;

	objects = objectLists[left][top];

	for( int i = left; i <= right; i++ )
		for( int j = top; j <= bottom; j++ )
			objects.insert( objects.end( ), objectLists[i][j].begin( ), objectLists[i][j].end( ) );

	return objects;
}

const bool CollisionGrid::HasObjectsInGridSquare( int X, int Y )
{
	return objectLists[X][Y].size( ) > 0;
}
std::vector< GameObject* > CollisionGrid::GetObjectsInGridSquare( int X, int Y )
{
	return objectLists[X][Y];
}

void CollisionGrid::Add( int X, int Y, GameObject* object )
{
	if( X < 0 || X >= objectLists.size( ) ||
		Y < 0 || Y >= objectLists[0].size( ) )
		return;

	vector< GameObject* >* vec = &objectLists[X][Y];

	if( std::find( vec->begin( ), vec->end( ), object ) != vec->end( ) )
		return;

	objectLists[X][Y].push_back( object );
}
void CollisionGrid::Remove( int X, int Y, GameObject* object )
{
	if( X < 0 || X >= objectLists.size( ) ||
		Y < 0 || Y >= objectLists[0].size( ) )
		return;

	vector< GameObject* >* vec = &objectLists[X][Y];

	vec->erase( std::remove( vec->begin( ), vec->end( ), object ), vec->end( ) );
}