#pragma once

#include "LuaClassWrapper.h"

namespace LuaScripting
{
	class LuaWrapper_Enemy : public LuaClassWrapper
	{
		public:
		static void ExposeFunctions( lua_State* script );

		private:
		static void CreateLifebar( const std::string fileName, const int enemyID );
	};
}