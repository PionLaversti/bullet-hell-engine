#pragma once

#include <SDL_events.h>
#include <SDL_keyboard.h>
#include <SDL_keycode.h>
#include <string>

namespace GameSystems
{
	enum KeyState
	{
		KEY_INVALID = -1,
		KEY_UP = 0,
		KEY_PRESSED = 1,
		KEY_DOWN = 2,
		KEY_RELEASED = 3
	};

	class InputManager
	{
		public:
		static void Initialize( );
		static void Update( );

		static KeyState GetKeyState( const std::string keyCode );
		static KeyState GetKeyState( const SDL_Scancode keyCode );

		static bool IsKeyUp( const std::string keyCode );
		static bool IsKeyPressed( const std::string keyCode );
		static bool IsKeyDown( const std::string keyCode );
		static bool IsKeyReleased( const std::string keyCode );

		static bool KeyUp( const SDL_Scancode keyCode );
		static bool KeyPressed( const SDL_Scancode keyCode );
		static bool KeyDown( const SDL_Scancode keyCode );
		static bool KeyReleased( const SDL_Scancode keyCode );

		private:
		static SDL_Scancode GetKeyCode( const std::string keyCode );

		static const int arraySize;
		static const Uint8* keyboardState;
		static Uint8 prevKeyboardState[];
	};
}