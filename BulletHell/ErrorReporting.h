#pragma once

#include <string>

namespace ErrorReporting
{
	void RaiseError( std::string errorMessage );
}