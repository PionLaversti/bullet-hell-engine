#pragma once

enum GL_Blend_Mode : int
{
	BLEND_NULL = 0,
	BLEND_ALPHA,
	BLEND_ADD
};