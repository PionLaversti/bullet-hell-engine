#pragma once

#include <string>
#include "Bullet.h"

namespace GameObjects
{
	namespace Projectiles
	{
		class ProgrammableBullet : public Bullet
		{
			public:
			ProgrammableBullet( );

			void Update( );

			std::string GetGraphic( ) const { return bulletTypeAlias; }
			float GetMaxSpeed( ) const { return maxSpeed; }
			float GetAcceleration( ) const { return acceleration; }
			float GetAngleAcceleration( ) const { return angleAcceleration; }
			float GetXSpeed( ) const { return xSpeed; }
			float GetYSpeed( ) const { return ySpeed; }
			float GetXAccel( ) const { return xAccel; }
			float GetYAccel( ) const { return yAccel; }
			float GetXMaxSpeed( ) const { return xMaxSpeed; }
			float GetYMaxSpeed( ) const { return yMaxSpeed; }

			void SetGraphic( const std::string bulTypeAlias );
			void SetX( float newX ) { xPosition = newX; }
			void SetY( float newY ) { yPosition = newY; }

				// 
			void SetSpeed( const float newSpeed );
			void SetAcceleration( const float newAccel );
			void SetMaxSpeed( const float newMaxSpeed );
			void SetAngle( const float newAngle );
			void SetAngleAcceleration( const float newAngleAccel );

			void SetXSpeed( const float newXSpeed );
			void SetYSpeed( const float newYSpeed );
			void SetXAccel( const float newXAccel );
			void SetYAccel( const float newYAccel );
			void SetXMaxSpeed( const float newXMaxSpeed );
			void SetYMaxSpeed( const float newYMaxSpeed );
			void SetXYSpeed( const float newXSpeed, const float newYSpeed );
			void SetXYAccel( const float newXAccel, const float newYAccel );
			void SetXYMaxSpeed( const float newXMaxSpeed, const float newYMaxSpeed );

			private:
			void ZeroOutPolarMovement( );
			void ZeroOutOrthagonalMovement( );

			std::string bulletTypeAlias;
			bool polarMovement;

			float acceleration;
			float angleAcceleration;
			float maxSpeed;

			float xAccel;
			float yAccel;
			float xMaxSpeed;
			float yMaxSpeed;
		};
	}
}