#include "Actor.h"
#include "LuaScript.h"

using namespace GameObjects;
using namespace LuaScripting;

Actor::Actor( const std::string scriptFileName ) : GameObject( )
{
	script = new LuaScript( scriptFileName );
}

void Actor::Preprocessing( )
{
	script->Preprocessing( );
}
void Actor::Initialize( )
{
	GameObject::Initialize( );

	script->Initialize( );
}
void Actor::Update( )
{
	GameObject::Update( );

	if( !active )
		return;

	script->Update( );
}
void Actor::Render( )
{
	if( !active )
		return;

	script->Render( );
}
void Actor::ShutDown( )
{
	GameObject::ShutDown( );

	script->ShutDown( );
}