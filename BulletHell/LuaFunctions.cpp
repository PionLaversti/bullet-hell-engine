#include "ErrorReporting.h"
#include "LuaFunctions.h"
#include "LuaLibraries.h"

#include <locale>
#include <codecvt>
#include <string>

using namespace luabridge;
using namespace LuaScripting;
using namespace std;

lua_State* LuaFunctions::OpenScript( string fileName )
{	
	lua_State* script = lua_open( );

	luaL_openlibs( script );
	
	int errorcode = luaL_dofile( script, fileName.c_str( ) );

	if( errorcode != 0 )
		LogError( script );

	RegisterScriptFileName( script, fileName );
	ExposeLibraries( script );

	return script;
}

void LuaFunctions::CloseScript( lua_State* script )
{
	lua_close( script );
}

void LuaFunctions::LogError( lua_State* script )
{
	std::string errorText = (string) lua_tostring( script, -1 );

	ErrorReporting::RaiseError( errorText );

	exit( EXIT_FAILURE );
}

void LuaFunctions::CallFunction(lua_State* script, string funct)
{
	lua_getglobal( script, funct.c_str( ) );

	int errorCode = lua_pcall( script, 0, 0, 0 );

	if( errorCode != 0 )
		LogError( script );
}

void LuaFunctions::ExposeLibraries( lua_State* script )
{
	LuaLibraries::ExposeFunctions( script );
}

void LuaFunctions::RegisterFunction( lua_State* script, const string functionName, lua_CFunction function )
{
	lua_pushcfunction( script, function );
	lua_setglobal( script, functionName.c_str() );
}

void PrintTable( lua_State* script, string tableName )
{
	string str;
	wstring wstr;
	const wchar_t* c_str;

	wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;

	lua_pushnil( script );

	while( lua_next(script, -2) != 0 )
	{
		if( lua_isfunction( script, -1 ) )
			str = tableName + string(".") + lua_tostring( script, -2 ) + string ( "\n" );
		if( lua_isstring( script, -1 ) )
			str = tableName + string(".") + lua_tostring( script, -2 ) + string(" : ") + lua_tostring( script, -1 ) + string ( "\n" );
		if( lua_isnumber( script, -1 ) )
			str = tableName + string(".") + lua_tostring( script, -2 ) + string(" : ") + to_string(lua_tonumber( script, -1 )) + string ( "\n" );
		if( lua_istable( script, -1 ) )
			PrintTable( script, tableName + string(".") + lua_tostring( script, -2 ) );
			
		wstr = converter.from_bytes( str.c_str( ) );
		c_str = wstr.c_str( );

		OutputDebugString( (LPCWSTR) c_str );

		lua_pop( script, 1 );
	}
}

void PrintStack( lua_State* script )
{
	int ind;
	int top = lua_gettop( script );

	string str;
	wstring wstr;
	const wchar_t* c_str;

	wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;

	for( ind = 1; ind <= top; ind++ )
	{
		int type = lua_type( script, ind );

		switch( type )
		{
			case LUA_TSTRING: str = to_string( ind ) + string( "- STRING: " ) + lua_tostring( script, ind ) + string( "\n" ); break;
			case LUA_TBOOLEAN: str = to_string( ind ) + string( "- BOOL: " ) + to_string( lua_toboolean( script, ind )) + string( "\n" ); break;
			case LUA_TNUMBER: str = to_string( ind ) + string( "- STRING: " ) + to_string( lua_tonumber( script, ind )) + string( "\n" ); break;
			default: str = to_string( ind ) + string( "- OTHER: " ) + lua_typename( script, type ) + string( "\n" ); break;
		}

		wstr = converter.from_bytes( str.c_str( ) );
		c_str = wstr.c_str( );

		OutputDebugString( (LPCWSTR) c_str );
	}
}

void LuaFunctions::RegisterFunction( lua_State* script, const string tableName, const string functionName, lua_CFunction function )
{
	lua_getglobal( script, tableName.c_str( ) );

	assert( lua_istable( script, -1 ) );

	lua_pushcfunction( script, function, 1 );

	PrintStack( script );

	rawsetfield( script, -2, functionName.c_str() );

	PrintStack( script );
}

template <class FP>
Namespace& addFunction (char const* name, FP const fp)
{
	assert (lua_istable (L, -1));

	new (lua_newuserdata( L, sizeof( fp ) )) FP( fp );
	lua_pushcclosure( L, &CFunc::Call <FP>::f, 1 );
	rawsetfield (L, -2, name);

	return *this;
}

string LuaFunctions::GetScriptNameAndLineNumber( lua_State* script )
{
	lua_Debug ar;
	lua_getstack( script, 1, &ar );
	lua_getinfo( script, "nSluf", &ar );

	return ar.short_src + string( ":" ) + to_string( ar.currentline );
}

string LuaFunctions::GetArgumentType( lua_State* script, const int argumentNumber )
{
	if( lua_isboolean( script, argumentNumber ) )
		return "boolean";
	if( lua_isfunction( script, argumentNumber ) )
		return "function";
	if( lua_islightuserdata( script, argumentNumber ) )
		return "userdata";
	if( lua_isnil( script, argumentNumber ) )
		return "nil";
	if( lua_isnone( script, argumentNumber ) )
		return "no value";
	if( lua_isnumber( script, argumentNumber ) )
		return "number";
	if( lua_isstring( script, argumentNumber ) )
		return "string";
	if( lua_istable( script, argumentNumber ) )
		return "table";
	if( lua_isthread( script, argumentNumber ) )
		return "thread";
	if( lua_isuserdata( script, argumentNumber ) )
		return "userdata";
}

void LuaFunctions::MatchArguments( lua_State* script, string functionName, int numArgs ... )
{
	va_list	 arguments;
	
	va_start( arguments, numArgs );

	for( int ind = 0; ind < numArgs; ind++ )
	{
		string typeNeeded = string( va_arg( arguments, char* ));
		string typeReceived = GetArgumentType( script, ind + 1 );

		if( typeNeeded != typeReceived )
			ErrorReporting::RaiseError( GetScriptNameAndLineNumber( script )
										+ ": bad argument #"
										+ to_string( ind + 1 )
										+ " to \'"
										+ functionName
										+ "\' ("
										+ typeNeeded
										+ " expected, got "
										+ LuaFunctions::GetArgumentType( script, 1 )
										+ ")" );
	}

	va_end( arguments );
}

void LuaFunctions::RegisterScriptFileName( lua_State* script, const std::string fileName )
{
	lua_pushstring( script, fileName.c_str( ) );
	lua_setglobal( script, "ScriptFileName" );
}

std::string LuaFunctions::GetScriptName( lua_State* script )
{
	lua_getglobal( script, "ScriptFileName" );
	std::string name = lua_tostring( script, -1 );
	lua_pop( script, 1 );

	return name;
}