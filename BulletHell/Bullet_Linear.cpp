#include "Bullet_Linear.h"

using namespace GameObjects::Projectiles;

Bullet_Linear::Bullet_Linear( ) : Bullet( ) { }

Bullet_Linear::Bullet_Linear( const float X, const float Y, const float spd, const float ang, ProjectileType* bulType )
	: Bullet( X, Y, spd, ang, bulType )
{
}