#pragma once

#include "Bullet.h"

namespace GameObjects
{
	namespace Projectiles
	{
		class Bullet_LinearAccel : public Bullet
		{
			public:
			Bullet_LinearAccel( );
			Bullet_LinearAccel( const float xStart, const float yStart, const float startSpeed, const float acceleration,
								const float maxSpeed, const float angle, ProjectileType* bulType );

			void Update( );

			private:
			float acceleration;
			float maxSpeed;
		};
	}
}