#pragma once

#include "LuaClassWrapper.h"

namespace LuaScripting
{
	class LuaWrapper_ProgrammableBullet : public LuaClassWrapper
	{
		public:
		static void ExposeFunctions( lua_State* script );
	};
}