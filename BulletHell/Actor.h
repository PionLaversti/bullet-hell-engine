#pragma once

namespace LuaScripting { class LuaScript; }

#include <string>
#include "GameObject.h"

namespace GameObjects
{
	class Actor : public GameObject
	{
		public:
		Actor( const std::string scriptFileName );

		virtual void Preprocessing( );
		virtual void Initialize( );
		virtual void Update( );
		virtual void Render( );
		virtual void ShutDown( );

		protected:
		LuaScripting::LuaScript* script;
		bool invincible;
	};
}