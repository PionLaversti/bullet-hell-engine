#pragma once

#include <math.h>
#include <SDL_rect.h>

namespace GameObjects
{
	class GameObject
	{
		public:
		GameObject( );
		virtual void Initialize( );
		virtual void Update( );
		virtual void ShutDown( );
		virtual void Destroy( );

		virtual float GetX( ) const { return xPosition; }
		virtual float GetY( ) const { return yPosition; }
		virtual void SetX( float newX ) { xPosition = newX; }
		virtual void SetY( float newY ) { yPosition = newY; }

		virtual bool IsActive( ) const { return active; }

		virtual SDL_Rect GetCollisionRect( );
		virtual void MoveBy( const float xDelta, const float yDelta );

		virtual void SetCollisionSize( float width, float height );
		virtual void SetCollisionRect( float X, float Y, float width, float height );

		protected:
		bool active;
		float collisionWidth, collisionHeight;
		float collisionXOffset, collisionYOffset;
		float xPosition, yPosition;

		int leftGridSquareID;
		int rightGridSquareID;
		int upperGridSquareID;
		int lowerGridSquareID;
	};
}