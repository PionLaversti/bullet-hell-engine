#include "Player.h"
#include "LuaScript.h"

using namespace GameObjects;
using namespace LuaScripting;

Player::Player(const std::string scriptName ) : Actor( scriptName )
{
	xPosition = 320;
	yPosition = 400;
	normalSpeed = 4.0f;
	slowSpeed = 1.5f;
	focussed = false;

	SetCollisionSize( 3, 3 );
}

void Player::Preprocessing( )
{
	script->Preprocessing( );
	script->RegisterEntity( this, "Player" );
}

void Player::MoveLeft()
{
	if( focussed )
		xPosition -= slowSpeed;
	else
		xPosition -= normalSpeed;
}
void Player::MoveRight()
{
	if( focussed )
		xPosition += slowSpeed;
	else
		xPosition += normalSpeed;
}
void Player::MoveDown()
{
	if( focussed )
		yPosition += slowSpeed;
	else
		yPosition += normalSpeed;
}
void Player::MoveUp()
{
	if( focussed )
		yPosition -= slowSpeed;
	else
		yPosition -= normalSpeed;
}

void Player::Shoot( )
{
	script->CallFunction( "Shoot" );
}