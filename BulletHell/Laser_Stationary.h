#pragma once

#include "ProjectileType.h"
#include "Laser.h"

namespace GameObjects
{
	namespace Projectiles
	{
		class Laser_Stationary : public Laser
		{
			public:
			Laser_Stationary( );
			Laser_Stationary( const float newX, const float newY, const float newAngle, const int newLength, const int newWidth, const int newWarningTime, const int newLifetime, ProjectileType* const laserType );
			~Laser_Stationary( );
		};
	}
}