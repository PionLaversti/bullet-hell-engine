#pragma once

#include "LuaClassWrapper.h"

namespace LuaScripting
{
	class LuaWrapper_Player : public LuaClassWrapper
	{
		public:
		static void ExposeFunctions( lua_State* script );
	};
}