#pragma once

#include "Bullet.h"

namespace GameObjects
{
	namespace Projectiles
	{
		class Bullet_Linear : public Bullet
		{
			public:
			Bullet_Linear( );
			Bullet_Linear( const float xStart, const float yStart, const float speed, const float angle, ProjectileType* bulType );
		};
	}
}