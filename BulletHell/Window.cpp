#include "Window.h"

#include "Constants.h"
#include "ImageWrapper.h"
#include "ResourceManager.h"

SDL_Surface* Window::screen;

//	--	--	--	--	--	--	--	--	--	--	--	--	--	--	--
//		Window::Window()
//	Window Initializer
//	--	--	--	--	--	--	--	--	--	--	--	--	--	--	--
Window::Window()
{
	screen = NULL;
}

//	--	--	--	--	--	--	--	--	--	--	--	--	--	--	--
//		Window::Initialize()
//	Initializes the window.
//	--	--	--	--	--	--	--	--	--	--	--	--	--	--	--
bool Window::Initialize( )
{
	window = SDL_CreateWindow( "Shmup Engine", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, _DEFAULT_SCREEN_WIDTH, _DEFAULT_SCREEN_HEIGHT, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN );

	SDL_SetWindowBordered( window, SDL_TRUE );

	// Initialize the screen.
	screen = SDL_GetWindowSurface( window );

	assert( screen != NULL );

	if( !InitOpenGL( ) )
		return false;

	// Everything initialized fine.
	return true;
}

//	--	--	--	--	--	--	--	--	--	--	--	--	--	--	--
//		Window::Draw()
//	Draws the specified texture to the screen.
//	At offset x, y.
//	--	--	--	--	--	--	--	--	--	--	--	--	--	--	--
//	PRECONDITION : A valid texture has been passed.
//	--	--	--	--	--	--	--	--	--	--	--	--	--	--	--
void Window::Render( int _x, int _y, SDL_Surface * _srcTex, SDL_Rect* _clipRect = NULL )
{
	glMatrixMode( GL_MODELVIEW );
	glLoadIdentity();

	glTranslatef( (GLfloat) _x, (GLfloat) _y, 0.0f );
}

void Window::PrintDebugText( const int X, const int Y, const std::string text )
{
	SDL_Color textColour = { 255, 255, 255 };
	SDL_Surface* textSurface = TTF_RenderUTF8_Blended( ResourceManager::GetFont( "lucon.ttf" ), text.c_str( ), textColour );

	glLoadIdentity( );

	GLuint texture;
	glGenTextures( 1, &texture );
	glBindTexture( GL_TEXTURE_2D, texture );

	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
	glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA, textSurface->w, textSurface->h, 0, GL_BGRA, GL_UNSIGNED_BYTE, textSurface->pixels );

	glBegin( GL_QUADS );

		glTexCoord2f( 0, 0 ); glVertex2f( X, Y );
		glTexCoord2f( 1, 0 ); glVertex2f( X + textSurface->w, Y );
		glTexCoord2f( 1, 1 ); glVertex2f( X + textSurface->w, Y + textSurface->h );
		glTexCoord2f( 0, 1 ); glVertex2f( X, Y + textSurface->h );
	
	glEnd( );

	glDeleteTextures( 1, &texture );

	SDL_Rect rect = { X, Y, textSurface->w, textSurface->h };

	SDL_BlitSurface( textSurface, NULL, screen, &rect );
	SDL_FreeSurface( textSurface );
}

//	--	--	--	--	--	--	--	--	--	--	--	--	--	--	--
//		Window::Clear()
//	Shows the current back-buffer to the screen
//	--	--	--	--	--	--	--	--	--	--	--	--	--	--	--
void Window::ClearWindow( )
{
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
}

//	--	--	--	--	--	--	--	--	--	--	--	--	--	--	--
//	 Window::SetWindowCaption()
//	Sets the current window's caption and icon.
//	--	--	--	--	--	--	--	--	--	--	--	--	--	--	--
void Window::SetWindowCaption( std::string _caption, std::string _icon_filename )
{
		// If they're NULL, set them to empty.
	if( _caption.length() == 0 )
		_caption = "";
	if( _icon_filename.length() == 0 )
		_icon_filename = "";
}

//	--	--	--	--	--	--	--	--	--	--	--	--	--	--	--
//	 Window::UpdateWindow()
//	Simply flips the window's buffers.
//	--	--	--	--	--	--	--	--	--	--	--	--	--	--	--
void Window::UpdateWindow()
{
	SDL_GL_SwapWindow(window);
}

bool Window::InitOpenGL()
{
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 1);

	glContext = SDL_GL_CreateContext(window);

	// Initialize OpenGL attributes
	SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);

	SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 16);
	SDL_GL_SetAttribute(SDL_GL_BUFFER_SIZE, 32);

	SDL_GL_SetAttribute(SDL_GL_ACCUM_RED_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_ACCUM_GREEN_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_ACCUM_BLUE_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_ACCUM_ALPHA_SIZE, 8);

	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 2);

	// Initialize Render matrices
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0.0, _DEFAULT_SCREEN_WIDTH, _DEFAULT_SCREEN_HEIGHT, 0, 1.0, -1.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	// Clear to this colour when ClearWindow is called.
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	// Viewport
	glViewport(0, 0, 640, 480);
	// Set Viewport up for 2D rendering
	glOrtho(0, 640, 480, 0, 1, -1);

	glEnable(GL_TEXTURE_2D);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);

	GLenum error = glGetError();

	return true;
}

void Window::SwitchBlendMode(GL_Blend_Mode _mode)
{
	switch (_mode)
	{
		case(BLEND_ALPHA) :
			glEnable( GL_ALPHA_TEST ); glEnable( GL_BLEND ); glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA ); break;
		case(BLEND_ADD) :
			glEnable( GL_BLEND ); glBlendFunc( GL_ONE, GL_ONE ); break;
	}
}