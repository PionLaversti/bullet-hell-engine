#include "Laser.h"

#include "DrawData.h"
#include "GLWrapper.h"
#include "ProjectileType.h"

using namespace GameObjects;
using namespace GameObjects::Projectiles;

Laser::Laser( )
{

}
Laser::Laser( const float newX, const float newY, const float newAngle, const int newLength, const int newWidth, const int newWarningTime, const int newLifetime, ProjectileType* const type )
{
	startX  = newX;
	startY = newY;
	angle  = newAngle;
	length = newLength;
	width  = newWidth;
	laserType	= type;
	warningTime = newWarningTime;
	lifetime = newLifetime;
	isActive = true;
}

const DrawData Laser::GetDrawData( )
{
	DrawData tmpData;
	SDL_Rect clipArea = SDL_Rect( laserType->graphicRect );

	tmpData.blitZone.x =	startX;
	tmpData.blitZone.y =	startY;
	tmpData.blitZone.h =	length;

	if( warningTime > 0 )
		tmpData.blitZone.w =	__max( (float) width / warningTime, 2 );
	else
		tmpData.blitZone.w =	width;

	tmpData.clipZone =	clipArea;

	tmpData.angle =			angle;

	tmpData.shouldRotate = true;
	tmpData.shouldStretch = false;

	if( laserType->addBlend )
		tmpData.blendMode = BLEND_ADD;
	else
		tmpData.blendMode = BLEND_ALPHA;

	tmpData.alpha = 0.8f;

	return tmpData;
}

void Laser::Update( )
{
	if( warningTime > 0 )
		warningTime--;
	else
	{
		if( lifetime == 0 )
			ShutDown( );

		if( lifetime > 0 )
			lifetime--;
	}
}
void Laser::ShutDown()
{
	isActive = false;
}