#include "Input.h"
#include <vector>
#include <Windows.h>

using namespace GameSystems;
using namespace std;

const int InputManager::arraySize = 512;
const Uint8* InputManager::keyboardState;
Uint8 InputManager::prevKeyboardState[ arraySize ];

void InputManager::Initialize( )
{
	keyboardState = SDL_GetKeyboardState( NULL );
}
void InputManager::Update( )
{
	std::copy( keyboardState, keyboardState + (arraySize - 1), prevKeyboardState );

	SDL_PumpEvents( );

	keyboardState = SDL_GetKeyboardState( NULL );
}

KeyState InputManager::GetKeyState( const string keyString )
{
	SDL_Scancode keyCode = GetKeyCode( keyString );

	return GetKeyState( keyCode );
}
KeyState InputManager::GetKeyState( const SDL_Scancode keyCode )
{
	if( KeyUp( keyCode ) )
		return KEY_UP;
	if( KeyPressed( keyCode ) )
		return KEY_PRESSED;
	if( KeyDown( keyCode ) )
		return KEY_DOWN;
	if( KeyReleased( keyCode ) )
		return KEY_RELEASED;

	throw new invalid_argument( "Did not recognize key code " + keyCode );
}

bool InputManager::IsKeyUp( const string keyString )
{
	SDL_Scancode keyCode = GetKeyCode( keyString );

	return KeyUp( keyCode );
}
bool InputManager::IsKeyPressed( const string keyString )
{
	SDL_Scancode keyCode = GetKeyCode( keyString );

	return KeyPressed( keyCode );
}
bool InputManager::IsKeyDown( const string keyString )
{
	SDL_Scancode keyCode = GetKeyCode( keyString );

	return KeyDown( keyCode );
}
bool InputManager::IsKeyReleased( const string keyString )
{
	SDL_Scancode keyCode = GetKeyCode( keyString );

	return KeyReleased( keyCode );
}

bool InputManager::KeyUp( const SDL_Scancode keyCode )
{
	return ( !keyboardState[keyCode] && !prevKeyboardState[keyCode] );
}
bool InputManager::KeyPressed( const SDL_Scancode keyCode )
{
	return ( keyboardState[keyCode] && !prevKeyboardState[keyCode] );
}
bool InputManager::KeyDown( const SDL_Scancode keyCode )
{
	return ( keyboardState[keyCode] && prevKeyboardState[keyCode] );
}
bool InputManager::KeyReleased( const SDL_Scancode keyCode )
{
	return ( !keyboardState[keyCode] && prevKeyboardState[keyCode] );
}

SDL_Scancode InputManager::GetKeyCode( const string keyString )
{
	if( keyString == "VK_LEFT" )
		return SDL_SCANCODE_LEFT;
	if( keyString == "VK_RIGHT" )
		return SDL_SCANCODE_RIGHT;
	if( keyString == "VK_UP" )
		return SDL_SCANCODE_UP;
	if( keyString == "VK_DOWN" )
		return SDL_SCANCODE_DOWN;
	if( keyString == "VK_SHOOT" )
		return SDL_SCANCODE_Z;
	if( keyString == "VK_BOMB" )
		return SDL_SCANCODE_X;
	if( keyString == "VK_FOCUS" )
		return SDL_SCANCODE_LSHIFT;

	if( keyString == "VK_DEBUG_INVULN" )
		return SDL_SCANCODE_I;

	throw new invalid_argument( "Did not recognize key code " + keyString + "." );
}