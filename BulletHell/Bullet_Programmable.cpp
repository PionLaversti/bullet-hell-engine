#include "Bullet_Programmable.h"

#include "ProjectileDataManager.h"
#include "ProjectileType.h"

using namespace GameObjects::Projectiles;
using namespace GameSystems;
using namespace std;

ProgrammableBullet::ProgrammableBullet( ) : Bullet( )
{
	polarMovement = true;
}

void ProgrammableBullet::Update( )
{
	Bullet::Update( );

	if( deathFramesLeft == -1 )
	{
		if( polarMovement )
		{
			if( acceleration != 0 )
			{
				speed += acceleration;

				if( (speed > 0 && speed > maxSpeed) || (speed < 0 && speed < maxSpeed) )
					speed = maxSpeed;
			}

			if( angleAcceleration != 0 )
			{
				angle += angleAcceleration;
			}

			xSpeed = speed * (float) cos( angle * (M_PI / 180) );
			ySpeed = speed * (float) sin( angle * (M_PI / 180) );
		}
		else
		{
			xSpeed += xAccel;
			ySpeed += yAccel;

			if( (xAccel > 0 && xSpeed > xMaxSpeed) || (xAccel < 0 && xSpeed < xMaxSpeed) )
				xSpeed = xMaxSpeed;
			if( (yAccel > 0 && ySpeed > yMaxSpeed) || (yAccel < 0 && ySpeed < yMaxSpeed) )
				ySpeed = yMaxSpeed;
		}
	}
}

void ProgrammableBullet::SetGraphic( string bulTypeAlias )
{
	bulletTypeAlias = bulTypeAlias;
	bulletType = ProjectileDataManager::FindByAlias( bulTypeAlias );
}
void ProgrammableBullet::SetSpeed( const float newSpeed )
{
	ZeroOutOrthagonalMovement( );

	speed = newSpeed;

}
void ProgrammableBullet::SetAcceleration( const float newAccel )
{
	ZeroOutOrthagonalMovement( );

	acceleration = newAccel;
}
void ProgrammableBullet::SetMaxSpeed( const float newMaxSpeed )
{
	ZeroOutOrthagonalMovement( );

	maxSpeed = newMaxSpeed;
}
void ProgrammableBullet::SetAngle( const float newAngle )
{
	ZeroOutOrthagonalMovement( );

	angle = newAngle;
}
void ProgrammableBullet::SetAngleAcceleration( const float newAngleAccel )
{
	ZeroOutOrthagonalMovement( );

	angleAcceleration = newAngleAccel;
}

void ProgrammableBullet::SetXSpeed( const float newXSpeed )
{
	ZeroOutPolarMovement( );

	xSpeed = newXSpeed;
}
void ProgrammableBullet::SetYSpeed( const float newYSpeed )
{
	ZeroOutPolarMovement( );

	ySpeed = newYSpeed;
}
void ProgrammableBullet::SetXAccel( const float xAcc )
{
	ZeroOutPolarMovement( );

	xAccel = xAcc;
}
void ProgrammableBullet::SetYAccel( const float yAcc )
{
	ZeroOutPolarMovement( );

	yAccel = yAcc;
}
void ProgrammableBullet::SetXMaxSpeed( const float xMax )
{
	ZeroOutPolarMovement( );
	
	xMaxSpeed = xMax;
}
void ProgrammableBullet::SetYMaxSpeed( const float yMax )
{
	ZeroOutPolarMovement( );

	yMaxSpeed = yMax;
}
void ProgrammableBullet::SetXYSpeed( const float xSpd, const float ySpd )
{
	ZeroOutPolarMovement( );

	xSpeed = xSpd;
	ySpeed = ySpd;
}
void ProgrammableBullet::SetXYAccel( const float xAcc, const float yAcc )
{
	ZeroOutPolarMovement( );

	xAccel = xAcc;
	yAccel = yAcc;
}
void ProgrammableBullet::SetXYMaxSpeed( const float xMax, const float yMax )
{
	ZeroOutPolarMovement( );

	xMaxSpeed = xMax;
	yMaxSpeed = yMax;
}

void ProgrammableBullet::ZeroOutOrthagonalMovement( )
{
	polarMovement = true;

	xAccel = 0;
	yAccel = 0;
	xMaxSpeed = 0;
	yMaxSpeed = 0;
}
void ProgrammableBullet::ZeroOutPolarMovement( )
{
	polarMovement = false;

	speed = 0;
	acceleration = 0;
	maxSpeed = 0;
}