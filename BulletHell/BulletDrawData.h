#pragma once

#include <SDL_opengl.h>
#include <SDL_rect.h>

namespace GameObjects
{
	namespace Projectiles
	{
		struct BulletDrawData
		{
			float xPosition;
			float yPosition;
			int width;
			int height;
			float angle;
			float alpha;
			bool shouldRotate;
			bool alphaBlend;
			bool addBlend;
			SDL_Rect clipZone;
		};
	}
}