#include "LuaWrapper_Sound.h"

#include "LuaLibraries.h"
#include "ResourceManager.h"
#include "SoundWrapper.h"

using namespace LuaScripting;

void LuaWrapper_Sound::ExposeFunctions( lua_State* script )
{
	luabridge::getGlobalNamespace( script )
		.beginNamespace( "Sound" )
			.addFunction( "LoadMusic", &ResourceManager::LoadMusic )
			.addFunction( "PlayMusic", &LuaWrapper_Sound::PlayMusic )
			.addFunction( "PauseMusic", &SoundWrapper::PauseMusic )
			.addFunction( "ResumeMusic", &SoundWrapper::ResumeMusic )
			.addFunction( "StopMusic", &SoundWrapper::StopMusic )
			.addFunction( "LoadSE", &ResourceManager::LoadSE )
			.addFunction( "PlaySE", &LuaWrapper_Sound::PlaySE )
		.endNamespace( );;
}

	// Get the specified music track from the resource manager and play it.
void LuaWrapper_Sound::PlayMusic( const std::string fileName )
{
	Mix_Music* musicFile = ResourceManager::GetMusic( fileName );

	SoundWrapper::PlayMusic( musicFile );
}

	// Get the specified sound effect from the resource manager and play it.
void LuaWrapper_Sound::PlaySE( const std::string fileName )
{
	Mix_Chunk* soundEffect = ResourceManager::GetSE( fileName );

	SoundWrapper::PlaySE( soundEffect );
}