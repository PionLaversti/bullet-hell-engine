#include "Timer.h"

//	Timer initializer. Resets all values to 0/false.
Timer::Timer()
{
	startAt = 0;
	pausedAt = 0;
	paused = false;
	started = false;
}

//	Pauses the timer. Stores the current elapsed ticks.
void Timer::Pause()
{
		// Can't Pause a timer that's not started, or is already paused.
	assert( started );
	assert( !paused );
	
		// Pause timer.
	paused = true;

		// Calculate the time passed when the timer's paused.
	pausedAt = SDL_GetTicks() - startAt;
}

//	Unpauses the timer. So that it will calculate correctly,
//	 it also shifts the Start time for the clock, so that the
//	 time elapsed matches with the difference in current time
//	 and its time.
void Timer::Unpause()
{
		// Can't Unpause a timer not started and not paused.
	assert( started );
	assert( paused );

		// Unpause timer
	paused = false;

		// Move the starting ticks so that it will count up correctly.
	startAt = SDL_GetTicks() - pausedAt;

		// Reset Pause ticks
	pausedAt = 0;
}

//	Starts the timer.
void Timer::Start()
{
		// Can't Start a timer that's already running.
	assert( !started );

		// Start timer
	started = true;
	paused = false;

		// Get current clock time
	startAt = SDL_GetTicks();
}

//	Stops the timer. It is neither paused nor started.
void Timer::Stop()
{
	started = false;
	paused = false;
}

//	Resets the timer to its default.
void Timer::Reset()
{
	startAt = 0;
	pausedAt = 0;
	paused = false;
	started = false;
}

//	Gets the amount of ticks passed between the timer's Start
//	 and the current time.
Uint32 Timer::GetElapsedTime()
{
		// If it's not started, then it should be 0.
	if( !started )
		return (Uint32) 0;

		// If it's paused, return the ticks elapsed when paused.
		// Otherwise, return the current time, minus the time the timer started at.
	if( paused )
		return pausedAt;
	else
		return ( SDL_GetTicks() - startAt );
}

//	Returns TRUE if timer is paused. FALSE otherwise.
bool Timer::IsPaused()
{
	return paused;
}

//	Returns TRUE if timer has started. FALSE otherwise.
bool Timer::IsStarted()
{
	return started;
}
