#pragma once

#include <boost/random.hpp>

namespace GameSystems
{
	class RandomNumberGenerator
	{
		public:
		static float RandomNumber( float lowBound, float highBound );
		static void SeedRNG( int seed );

		private:
		static boost::random::mt19937 RNG;
	};
}