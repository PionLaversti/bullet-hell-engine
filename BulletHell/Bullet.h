#pragma once

#include "GameObject.h"

namespace GameSystems
{
	class CollisionDetector;
	class CollisionGrid;
}

namespace GameObjects
{
	struct DrawData;

	namespace Projectiles
	{
		struct BulletDrawData;
		struct ProjectileType;

		class Bullet : public GameObject
		{
			public:
			Bullet( );
			Bullet( const float X, const float Y, const float speed, const float angle, ProjectileType* bulletType );

			virtual void Update( );
			virtual void ShutDown( );

			// Get_Draw_Data : Retrieves necessary data to Render the bullet with.
			virtual const DrawData GetDrawData( );

			virtual SDL_Rect GetCollisionRect( );

			// Get_Bullet_Type : Retrieves the bullet's ProjectileType, which contains data used elsewhere.
			virtual ProjectileType* const GetBulletType( ) { return bulletType; }
			// Delete_On_Screen_Exit : Whether or not the bullet should be deleted when it moves out of bounds.
			virtual const bool DeleteOnScreenExit( ) { return deleteOnScreenExit; }

			virtual float GetAngle( ) const { return angle; }
			virtual float GetSpeed( ) const { return speed; }

			protected:
			float xSpeed;
			float ySpeed;
			float speed;
			float angle;

			bool deleteOnScreenExit;
			ProjectileType* bulletType;

			int collisionGridLeft;
			int collisionGridTop;
			int collisionGridRight;
			int collisionGridBottom;

			static const int DEATH_FRAMES;
			int deathFramesLeft;

			private:
			static GameSystems::CollisionGrid* enemyBulletGrid;

			friend class ::GameSystems::CollisionDetector;
		};
	}
}