#pragma once

#include "Actor.h"

namespace GameObjects
{
	class Player : public Actor
	{
		public:
		Player( const std::string scriptName );

		void Preprocessing( );

		void MoveLeft( );
		void MoveRight( );
		void MoveUp( );
		void MoveDown( );
		void SetSlowMove( bool slow ) {focussed = slow; }
		void SetInvincibility( bool invuln ) { invincible = invuln; }
		void Shoot( );
		void Bomb( ) {}

		const float GetCurrentSpeed( ) { return (focussed ? slowSpeed : normalSpeed); }
		const bool IsFocussed( ) { return focussed; }
		const bool IsInvincible( ) { return invincible; };

		private:
		float normalSpeed;
		float slowSpeed;
		bool focussed;
	};
}