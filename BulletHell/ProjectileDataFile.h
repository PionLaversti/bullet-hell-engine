#pragma once

#include <map>
#include <string>
#include <vector>

#include "ProjectileType.h"

struct lua_State;
class Texture;

namespace luabridge
{
	class LuaRef;
}

namespace GameObjects
{
	namespace Projectiles
	{
		struct ProjectileType;
	}
}

namespace GameSystems
{
	class ProjectileDataFile
	{
		public:
		void LoadDataFile( lua_State* dataFile );
		void UnloadDataFile( );

		GameObjects::Projectiles::ProjectileType* FindByAlias( const std::string alias );
		Texture* GetBulletTexture( );
		const std::string GetLibraryName( );

		private:
		void LoadBulletData( luabridge::LuaRef bulletDataChunk, int ID );

		std::string						projectileLibraryName;
		std::map<std::string, int>		projectileAliases;
		std::vector< GameObjects::Projectiles::ProjectileType >	projectileTypes;
		Texture*						projectileImage;
	};
}