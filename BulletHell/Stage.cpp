#include "Stage.h"

using namespace GameObjects;
using namespace LuaScripting;

Stage::Stage( const std::string scriptFileName )
{
	script = new LuaScript( scriptFileName );
}

void Stage::Preprocessing( )
{
	script->Preprocessing( );
}

void Stage::Initialize( )
{
	script->Initialize( );
}

void Stage::Update( )
{
	script->Update( );
}

void Stage::RenderBackground( )
{
	script->CallFunction( "RenderBackground" );
}

void Stage::Render( )
{
	script->Render( );
}

void Stage::ShutDown( )
{
	script->ShutDown( );
}