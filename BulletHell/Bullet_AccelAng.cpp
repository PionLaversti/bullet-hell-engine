#include "Bullet_AccelAng.h"

using namespace GameObjects::Projectiles;

Bullet_AccelAng::Bullet_AccelAng( ) : Bullet( ) { }

Bullet_AccelAng::Bullet_AccelAng( const float X, const float Y, const float spd, const float ang,
								const float angDiff, ProjectileType* bulType )
	: Bullet( X, Y, spd, ang, bulType )
{
	angAccel		= angDiff;
}

void Bullet_AccelAng::Update( )
{
	Bullet::Update( );

	if( deathFramesLeft == -1 )
	{
		xPosition += xSpeed;
		yPosition += ySpeed;

		angle += angAccel;

		xSpeed = speed * (float) cos( angle * (M_PI / 180) );
		ySpeed = speed * (float) sin( angle * (M_PI / 180) );
	}
}