#include "LuaWrapper.h"

#include "LuaLibraries.h"

#include "LuaWrapper_ActorManager.h"
#include "LuaWrapper_Controls.h"
#include "LuaWrapper_Enemy.h"
#include "LuaWrapper_Etc.h"
#include "LuaWrapper_Graphics.h"
#include "LuaWrapper_Lifebar.h"
#include "LuaWrapper_Player.h"
#include "LuaWrapper_Projectiles.h"
#include "LuaWrapper_Sound.h"

#include "LuaWrapper_ProgrammableBullet.h"
#include "LuaWrapper_ProgrammableLaser.h"

using namespace LuaScripting;

bool LuaWrapper::Initialize( )
{
	InitializeLibraries( );

	return true;
}

void LuaWrapper::InitializeLibraries( )
{
	LuaLibraries::RegisterLibrary( &LuaWrapper_ActorManager::ExposeFunctions, "GENERAL" );
	LuaLibraries::RegisterLibrary( &LuaWrapper_Enemy::ExposeFunctions, "ENEMY" );
	LuaLibraries::RegisterLibrary( &LuaWrapper_Graphics::ExposeFunctions, "GRAPHICS" );
	LuaLibraries::RegisterLibrary( &LuaWrapper_Lifebar::ExposeFunctions, "LIFEBAR" );
	LuaLibraries::RegisterLibrary( &LuaWrapper_Player::ExposeFunctions, "PLAYER" );
	LuaLibraries::RegisterLibrary( &LuaWrapper_Projectiles::ExposeFunctions, "PROJECTILES" );
	LuaLibraries::RegisterLibrary( &LuaWrapper_Sound::ExposeFunctions, "SOUND" );

	LuaLibraries::RegisterLibrary( &LuaWrapper_ProgrammableBullet::ExposeFunctions, "PROJECTILES" );
	LuaLibraries::RegisterLibrary( &LuaWrapper_ProgrammableLaser::ExposeFunctions, "PROJECTILES" );

	LuaLibraries::RegisterLibrary( &LuaWrapper_Controls::ExposeFunctions, "CONTROLS" );

	LuaLibraries::RegisterLibrary( &LuaWrapper_Etc::ExposeFunctions, "GENERAL" );
}