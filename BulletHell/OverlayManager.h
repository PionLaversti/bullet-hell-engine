#pragma once

#include <list>

namespace GameObjects
{
	class Lifebar;
}

namespace GameSystems
{
	class OverlayManager
	{
		public:
		static void Register( GameObjects::Lifebar* lifebar );
		static void Unregister( GameObjects::Lifebar* lifebar );

		static void Initialize( );
		static void Update( );
		static void Render( );
		static void ShutDown( );

		private:
		static std::list<GameObjects::Lifebar*> overlays;
		static std::list<GameObjects::Lifebar*>::iterator iter;
	};
}