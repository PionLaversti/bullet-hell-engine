#include "LuaWrapper_Graphics.h"

#include "ErrorReporting.h"
#include "LuaLibraries.h"
#include "ResourceManager.h"
#include "Texture.h"

using namespace std;
using namespace LuaScripting;

void LuaWrapper_Graphics::ExposeFunctions( lua_State* script )
{
	luabridge::getGlobalNamespace( script )
		.beginNamespace( "Graphics" )
		.addFunction( "Load", &LuaWrapper_Graphics::LoadTexture )
		.addFunction( "Render", &LuaWrapper_Graphics::Render )
		.addFunction( "RenderToSize", &LuaWrapper_Graphics::RenderToSize )
		.addFunction( "RenderInRect", &LuaWrapper_Graphics::RenderInRect )
		.addFunction( "Unload", &LuaWrapper_Graphics::UnloadTexture )
		.endNamespace( );
}

void LuaWrapper_Graphics::LoadTexture( const string fileName )
{
	if( !ResourceManager::LoadTexture( fileName ) )
		ErrorReporting::RaiseError( "Could not load image: \"" + fileName + "\"" );
}

void LuaWrapper_Graphics::Render( const string fileName, const float X, const float Y )
{
	Texture* tex = ResourceManager::GetTexture( fileName );

	if( tex->GetTextureID() == 0 )
	{
		ErrorReporting::RaiseError( "Graphics.Render : \"" + fileName + "\" not loaded." );
		return;
	}

	tex->Render( X, Y );

	tex = nullptr;
}

void LuaWrapper_Graphics::RenderToSize( const string fileName, const float X, const float Y, const float width, const float height )
{
	Texture* tex = ResourceManager::GetTexture( fileName );

	if( tex->GetTextureID() == 0 )
	{
		ErrorReporting::RaiseError( "Graphics.RenderToSize : \"" + fileName + "\" not loaded." );
		return;
	}

	tex->RenderToSize( X, Y, width, height );

	tex = nullptr;
}

void LuaWrapper_Graphics::RenderInRect( const string fileName, const float left, const float top, const float right, const float bottom )
{
	Texture* tex = ResourceManager::GetTexture( fileName );

	if( tex->GetTextureID() == 0 )
	{
		ErrorReporting::RaiseError( "Graphics.RenderInRect : \"" + fileName + "\" not loaded." );
		return;
	}

	tex->RenderInRect( left, top, right, bottom );

	tex = nullptr;
}

void LuaWrapper_Graphics::UnloadTexture( const string fileName )
{
	ResourceManager::ReleaseTexture( fileName );
}