#pragma once

#include <SDL_rect.h>
#include <vector>

#include "GameObject.h"

namespace GameSystems
{
	class CollisionGrid
	{
		public:
		void Initialize( const int screenWidth, const int screenHeight );

		const bool HasObjectsInOverlappedSquares( SDL_Rect target );
		std::vector< GameObjects::GameObject* > GetObjectsInOverlappedSquares( SDL_Rect target );

		const bool HasObjectsInGridSquare( int X, int Y );
		std::vector< GameObjects::GameObject* > GetObjectsInGridSquare( int X, int Y );

		void Add( int X, int Y, GameObjects::GameObject* object );
		void Remove( int X, int Y, GameObjects::GameObject* object );

		static const int GetSquarePointLiesIn( const int val );

		static const int BUFFER_SQUARES = 10;
		static const int GRID_SIZE_PX = 32;

		static const int GRID_START_X = (GRID_SIZE_PX * -BUFFER_SQUARES);
		static const int GRID_START_Y = (GRID_SIZE_PX * -BUFFER_SQUARES);
		private:
		std::vector< std::vector< std::vector< GameObjects::GameObject* > > > objectLists;
	};
}