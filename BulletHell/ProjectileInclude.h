///	--	--	--	--	--	--	--	--	--	--	--	--	--	--	--	--	--	--	--	--
/// BulletInclude.h
///	Provides all classes that use this, with implementation methods of all bullets.
///	--	--	--	--	--	--	--	--	--	--	--	--	--	--	--	--	--	--	--	--

#pragma once

#include "BulletDrawData.h"
#include "ProjectileType.h"

#include "Bullet.h"
#include "Bullet_AccelAng.h"
#include "Bullet_AccelXY.h"
#include "Bullet_Linear.h"
#include "Bullet_LinearAccel.h"

#include "LaserDrawData.h"

#include "Laser.h"
#include "Laser_Stationary.h"
#include "Laser_Rotating.h"