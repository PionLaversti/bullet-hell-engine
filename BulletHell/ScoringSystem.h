#pragma once

namespace GameSystems
{
	class ScoringSystem
	{
		public:
		static void Initialize( );
		static void Update( );
		static void Render( );
		static void ShutDown( );

		static void ScorePoints( const int pointAmount ) { currentPoints += pointAmount * pointMultiplier; }
		static void ScoreUnscaledPoints( const int pointAmount ) { currentPoints += pointAmount; }
		static int GetCurrentPoints( ) { return currentPoints; }
		static int GetPointMultiplier( ) { return currentPoints; }

		private:
		static int currentPoints;
		static float pointMultiplier;
	};
}