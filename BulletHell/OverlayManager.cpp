#include "OverlayManager.h"
#include "Lifebar.h"

using namespace GameObjects;
using namespace GameSystems;
using namespace std;

list<Lifebar*> OverlayManager::overlays;
list<Lifebar*>::iterator OverlayManager::iter;

void OverlayManager::Register( GameObjects::Lifebar* lifebar )
{
	overlays.push_back( lifebar );
}
void OverlayManager::Unregister( GameObjects::Lifebar* lifebar )
{
	overlays.remove( lifebar );
}

void OverlayManager::Initialize( )
{
}
void OverlayManager::Update( )
{
	iter = overlays.begin( );

	while( iter != overlays.end( ) )
	{
		(*iter)->Update( );
		iter ++;
	}
}
void OverlayManager::Render( )
{
	iter = overlays.begin( );

	while( iter != overlays.end( ) )
	{
		(*iter)->Render( );
		iter ++;
	}
}
void OverlayManager::ShutDown( )
{
	iter = overlays.begin( );

	while( iter != overlays.end( ) )
	{
		(*iter)->ShutDown( );
		iter ++;
	}
}