#include "ProjectileDataFile.h"

#include "LuaInclude.h"
#include "ProjectileType.h"
#include "ResourceManager.h"
#include "Texture.h"

#include <sstream>
#include <locale>
#include <codecvt>
#include <string>

using namespace GameObjects::Projectiles;
using namespace GameSystems;
using namespace luabridge;
using namespace std;

void ProjectileDataFile::LoadDataFile( lua_State* dataFile )
{
	luabridge::LuaRef table = luabridge::LuaRef::getGlobal( dataFile, "BULLET_DATA_CONTAINER" );

	projectileLibraryName = LuaRef_cast<string>( table[ "bullet_library_name" ] );
	
	string bulletTextureName = LuaRef_cast<string>( table[ "bullet_texture" ] );

	ResourceManager::LoadTexture( bulletTextureName );
	projectileImage = ResourceManager::GetTexture( bulletTextureName );
	
	luabridge::LuaRef bulletDataTable = table["BULLETS"];

	if( bulletDataTable.isNil( ) )
		throw invalid_argument( "Could not find projectile data." );

	int ind = 1;
	size_t length = bulletDataTable.length( );

	projectileTypes.resize( length );

	while( ind <= length )
	{
		luabridge::LuaRef bulletDataChunk = bulletDataTable[ind];

		LoadBulletData( bulletDataChunk, ind );

		ind++;
	}
}
void ProjectileDataFile::UnloadDataFile()
{
	projectileImage->FreeTexture();
}

void ProjectileDataFile::LoadBulletData( luabridge::LuaRef bulletDataChunk, int ID )
{
	ProjectileType projectileType;

	int len = bulletDataChunk.length( );

	projectileType.collisionHeight =	luabridge::LuaRef_cast<int>( bulletDataChunk[ "collision_height" ] );
	projectileType.collisionWidth =		luabridge::LuaRef_cast<int>( bulletDataChunk[ "collision_width" ] );
	projectileType.shouldRotate =		luabridge::LuaRef_cast<bool>( bulletDataChunk[ "rotate" ] );

	luabridge::LuaRef graphicRect =		luabridge::LuaRef( bulletDataChunk[ "graphic_rect" ] );
	projectileType.graphicRect.x =		luabridge::LuaRef_cast<int>(graphicRect[1]);
	projectileType.graphicRect.y =		luabridge::LuaRef_cast<int>(graphicRect[2]);
	projectileType.graphicRect.w =		luabridge::LuaRef_cast<int>(graphicRect[3]);
	projectileType.graphicRect.h =		luabridge::LuaRef_cast<int>(graphicRect[4]);

	string blendType =					luabridge::LuaRef_cast<std::string>( bulletDataChunk[ "blend_mode" ] );

	if( blendType == "ALPHA" )
		projectileType.alphaBlend = true;
	else if( blendType == "ADD" )
		projectileType.addBlend = true;

	string alias =						luabridge::LuaRef_cast<std::string>( bulletDataChunk[ "alias" ] );

	projectileAliases[alias] = ID - 1;
	projectileTypes[ID - 1] = projectileType;
}

ProjectileType* ProjectileDataFile::FindByAlias( const std::string _alias )
{
	if( projectileAliases.count( _alias ) == 0 )
	{
		throw std::invalid_argument( "Could not find bullet type \"" + _alias + "\"" );
	}

	int bulID = projectileAliases[ _alias ];
	return &projectileTypes[ bulID ];
}
Texture* ProjectileDataFile::GetBulletTexture()
{ 
	return projectileImage;
}
const string ProjectileDataFile::GetLibraryName()
{ 
	return projectileLibraryName;
}