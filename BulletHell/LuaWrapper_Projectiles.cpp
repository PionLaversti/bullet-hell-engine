#include "LuaWrapper_Projectiles.h"

#include <string>

#include "ErrorReporting.h"
#include "LuaLibraries.h"
#include "ProjectileManager.h"
#include "ProjectileDataManager.h"

using namespace GameSystems;
using namespace LuaScripting;
using namespace std;

int CreateXYAccelBullet( lua_State* script )
{
	LuaFunctions::MatchArguments( script, "CreateXYAccelBullet", 9, "number", "number", "number", "number", "number", "number", "number", "number", "string" );

	ProjectileManager::CreateXYAccelBullet(
			lua_tonumber( script, 1 ),
			lua_tonumber( script, 2 ),
			lua_tonumber( script, 3 ),
			lua_tonumber( script, 4 ),
			lua_tonumber( script, 5 ),
			lua_tonumber( script, 6 ),
			lua_tonumber( script, 7 ),
			lua_tonumber( script, 8 ),
			lua_tostring( script, 9 )
		);

	return 0;
}

int CreateRotatingLaser( lua_State* script )
{
	LuaFunctions::MatchArguments( script, "CreateRotatingLaser", 9, "number", "number", "number", "number", "number", "number", "number", "number", "string" );

	ProjectileManager::CreateRotatingLaser(
		lua_tonumber( script, 1 ),
		lua_tonumber( script, 2 ),
		lua_tonumber( script, 3 ),
		lua_tonumber( script, 4 ),
		lua_tonumber( script, 5 ),
		lua_tonumber( script, 6 ),
		lua_tonumber( script, 7 ),
		lua_tonumber( script, 8 ),
		lua_tostring( script, 9 )
		);

	return 0;
}

void LuaWrapper_Projectiles::ExposeFunctions( lua_State* script )
{
	luabridge::getGlobalNamespace( script )
		.beginNamespace( "Projectiles" )
			.addFunction( "CreateLinearBullet", &ProjectileManager::CreateLinearBullet )
			.addFunction( "CreateLinearAccelBullet", &ProjectileManager::CreateLinearAccelBullet )
			.addFunction( "CreateAngularAccelBullet", &ProjectileManager::CreateAngularAccelBullet )
			.addFunction( "CreateStationaryLaser", &ProjectileManager::CreateStationaryLaser )
			.addFunction( "CreateBullet", &ProjectileManager::CreateProgrammableBullet )
			.addFunction( "CreateLaser", &ProjectileManager::CreateProgrammableLaser )
			.addFunction( "CreatePlayerBullet", &ProjectileManager::CreatePlayerBullet )
			.addFunction( "LoadProjectileData", &ProjectileDataManager::LoadDataFile )
		.endNamespace( );

		// These have to be registered manually, as they need too many arguments for luabridge.
	LuaFunctions::RegisterFunction( script, "Projectiles", "CreateXYAccelBullet", &CreateXYAccelBullet );
	LuaFunctions::RegisterFunction( script, "Projectiles", "CreateRotatingLaser", &CreateRotatingLaser );
}