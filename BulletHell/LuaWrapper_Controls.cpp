#include "LuaWrapper_Controls.h"

#include "Input.h"

using namespace GameSystems;
using namespace LuaScripting;
using namespace std;

void LuaWrapper_Controls::ExposeFunctions( lua_State* script )
{
	luabridge::getGlobalNamespace( script )
		.beginNamespace( "Controls" )
		.addFunction( "IsKeyPressed", &InputManager::IsKeyPressed )
		.addFunction( "IsKeyDown", &InputManager::IsKeyDown )
		.addFunction( "IsKeyReleased", &InputManager::IsKeyReleased )
		.addFunction( "IsKeyUp", &InputManager::IsKeyUp )
		.endNamespace( );
}