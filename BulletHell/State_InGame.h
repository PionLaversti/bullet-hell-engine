///	--	--	--	--	--	--	--	--	--	--	--	--	--	--	--	--	--	--	--	--
/// State_InGame.h
///	Used to play out the game from loaded scriptfiles.
///	--	--	--	--	--	--	--	--	--	--	--	--	--	--	--	--	--	--	--	--

#pragma once

namespace GameObjects
{
	class Stage;
}

namespace GameSystems
{
	namespace StateMachineSystem
	{
		class State_InGame : public GameState
		{
			public:
			// Loads resources, initializes things.
			State_InGame( const int playArea_width, const int playArea_height );
			// Frees resources.
			~State_InGame( );

			// Gameloop functions
			GameStateType GetNextState( ) { return nextState; }
			void HandleEvents( );
			void HandleLogic( );
			void Render( );

			// Resizes play area.
			void ResizeLevelSize( const int width, const int height );
			void ResizeLevelBounds_Player( const int size );
			void ResizeLevelBounds_Bullets( const int size );
			private:
			// Default level variables.
			const static int DEFAULT_WIDTH = 640;
			const static int DEFAULT_HEIGHT = 480;
			const static int DEFAULT_BULLET_BUFFER_DIST = 70;
			const static int DEFAULT_PLAYER_BUFFER_DIST = 16;

			// Actual level dimensions.
			int levelWidth;
			int levelHeight;

			// Bullet-bounds-buffer. If a bullet travels further than this distance from the visible
				// screen, it is deleted.
			int bulletBufferDistance;

			// Player-bounds-buffer. The player can move within the bounds of the visible screen,
				// minus this distance.
			int playerBufferDistance;

			GameObjects::Stage* stage;

			// Frames passed since it started.
			int frameCount;

			// Booleans that state how the player should move.
			bool playerMoveLeft;
			bool playerMoveRight;
			bool playerMoveUp;
			bool playerMoveDown;
			bool playerShoot;
			bool playerBomb;
			bool playerInvincibility;
			bool playerFocus;

			bool paused;
			bool pausedpressed;
		};
	}
}