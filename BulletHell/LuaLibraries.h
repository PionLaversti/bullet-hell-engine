#pragma once

#include <functional>
#include <map>
#include <vector>

#include "LuaInclude.h"

using namespace std::placeholders;

namespace LuaScripting
{
	class LuaLibraries
	{
		public:
		static void ExposeFunctions( lua_State* script );
		static void IncludeLibrary( const std::string libraryName, lua_State* script );
		static void RegisterLibrary( std::function<void( lua_State* )> library, std::string libraryName );
		static void RegisterScriptType( const std::string scriptType );

		private:
		static std::map< std::string, std::vector< std::function<void( lua_State* )> > > registeredLibraries;
	};
}