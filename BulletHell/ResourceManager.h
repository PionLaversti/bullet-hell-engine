#pragma once

#include <map>

#include <SDL_mixer.h>
#include <SDL_ttf.h>

class Texture;

class ResourceManager
{
public:
	static TTF_Font*	GetFont(std::string);
	static Mix_Music*	GetMusic(std::string);
	static Mix_Chunk*	GetSE(std::string);
	static Texture*		GetTexture(std::string);

	static bool LoadFont(std::string, int);
	static bool LoadMusic(std::string);
	static bool LoadSE(std::string);
	static bool LoadTexture(std::string);

	static void ReleaseFont(std::string);
	static void ReleaseMusic(std::string);
	static void ReleaseSE(std::string);
	static void ReleaseTexture(std::string);

	static void release_all();

private:
	static std::map< std::string, Mix_Chunk* >		lib_SE;
	static std::map< std::string, Mix_Music* >		lib_music;
	static std::map< std::string, Texture* >		lib_textures;
	static std::map< std::string, TTF_Font* >		lib_fonts;
};