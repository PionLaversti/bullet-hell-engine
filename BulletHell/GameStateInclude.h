///	--	--	--	--	--	--	--	--	--	--	--	--	--	--	--	--	--	--	--	--
/// GameStateInclude.h
///	Provides all classes that use this, with implementation methods of all gamestates.
///	--	--	--	--	--	--	--	--	--	--	--	--	--	--	--	--	--	--	--	--

#pragma once

#include "StateMachine.h"

#include "GameState.h"
#include "State_InGame.h"