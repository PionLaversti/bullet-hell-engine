#include "LuaWrapper_Lifebar.h"
#include "LuaLibraries.h"

#include "Lifebar.h"

using namespace GameObjects;
using namespace LuaScripting;

void LuaWrapper_Lifebar::ExposeFunctions( lua_State* script )
{
	luabridge::getGlobalNamespace( script )
		.beginClass<Lifebar>( "Lifebar" )
			.addFunction( "GetCurrentLife", &Lifebar::GetCurrentLife )
			.addFunction( "GetMaximumLife", &Lifebar::GetMaximumLife )
			.addFunction( "GetLifeRatio", &Lifebar::GetLifeRatio )
		.endClass( );
}