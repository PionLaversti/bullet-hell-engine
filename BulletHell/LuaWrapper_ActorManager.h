#pragma once

#include "LuaClassWrapper.h"

namespace LuaScripting
{
	class LuaWrapper_ActorManager : LuaClassWrapper
	{
		public:
		static void ExposeFunctions( lua_State* script );

		static float GetPlayerX( );
		static float GetPlayerY( );
		static float GetAngleToPlayer( const float X, const float Y );

		private:
	};
}