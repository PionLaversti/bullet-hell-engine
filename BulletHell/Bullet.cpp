#include "Bullet.h"

#include <math.h>

#include "CollisionGrid.h"
#include "DrawData.h"
#include "GLWrapper.h"
#include "ProjectileType.h"

using namespace GameObjects;
using namespace GameObjects::Projectiles;
using namespace GameSystems;

CollisionGrid* Bullet::enemyBulletGrid;
const int Bullet::DEATH_FRAMES = 30;

Bullet::Bullet( ) : GameObject( )
{
	active = false;
}

Bullet::Bullet( const float X, const float Y, const float spd, const float ang, ProjectileType* bulType ) : GameObject( )
{
	xPosition		= X;
	yPosition		= Y;
	speed			= spd;
	angle			= ang *  (float) (M_PI / 180);
	xSpeed		= spd *  (float)cos(angle);
	ySpeed		= spd *  (float)sin(angle);

	active = true;
	bulletType = bulType;
	deleteOnScreenExit = true;
	deathFramesLeft = -1;
}

void Bullet::Update()
{
	if( deathFramesLeft > 0 )
	{
		xSpeed = xSpeed * 0.95f;
		ySpeed = ySpeed * 0.95f;

		MoveBy( xSpeed, ySpeed );

		deathFramesLeft--;

		return;
	}

	if( deathFramesLeft == 0 )
		Destroy( );

	MoveBy( xSpeed, ySpeed );

	SDL_Rect rect = GetCollisionRect( );

	const int newLeft = CollisionGrid::GetSquarePointLiesIn( rect.x );
	const int newTop = CollisionGrid::GetSquarePointLiesIn( rect.y );
	const int newRight = CollisionGrid::GetSquarePointLiesIn( rect.x + rect.w );
	const int newBottom = CollisionGrid::GetSquarePointLiesIn( rect.y + rect.h );

	if( collisionGridTop != newTop || collisionGridBottom != newBottom || collisionGridLeft != newLeft || collisionGridRight != newRight )
	{
		enemyBulletGrid->Remove( collisionGridLeft, collisionGridTop, this );

		if( collisionGridLeft != collisionGridRight )
			enemyBulletGrid->Remove( collisionGridRight, collisionGridTop, this );
		if( collisionGridTop != collisionGridBottom )
			enemyBulletGrid->Remove( collisionGridLeft, collisionGridBottom, this );
		if( (collisionGridLeft != collisionGridRight) && (collisionGridTop != collisionGridBottom) )
			enemyBulletGrid->Remove( collisionGridRight, collisionGridBottom, this );

		if( newTop < 0 )
		{
			ShutDown( );
		}
		else
		{
			enemyBulletGrid->Add( newLeft, newTop, this );

			if( collisionGridLeft != collisionGridRight )
				enemyBulletGrid->Add( newRight, newTop, this );
			if( collisionGridTop != collisionGridBottom )
				enemyBulletGrid->Add( newLeft, newBottom, this );
			if( (collisionGridLeft != collisionGridRight) && (collisionGridTop != collisionGridBottom) )
				enemyBulletGrid->Add( newRight, newBottom, this );

			collisionGridLeft = newLeft;
			collisionGridRight = newRight;
			collisionGridBottom = newBottom;
			collisionGridTop = newTop;
		}
	}
}

void Bullet::ShutDown( )
{
	deathFramesLeft = DEATH_FRAMES;

	SDL_Rect rect = GetCollisionRect( );

	const int newLeft = CollisionGrid::GetSquarePointLiesIn( rect.x );
	const int newTop = CollisionGrid::GetSquarePointLiesIn( rect.y );
	const int newRight = CollisionGrid::GetSquarePointLiesIn( rect.x + rect.w );
	const int newBottom = CollisionGrid::GetSquarePointLiesIn( rect.y + rect.h );

	enemyBulletGrid->Remove( collisionGridLeft, collisionGridTop, this );

	if( collisionGridLeft != collisionGridRight )
		enemyBulletGrid->Remove( collisionGridRight, collisionGridTop, this );
	if( collisionGridTop != collisionGridBottom )
		enemyBulletGrid->Remove( collisionGridLeft, collisionGridBottom, this );
	if( (collisionGridLeft != collisionGridRight) && (collisionGridTop != collisionGridBottom) )
		enemyBulletGrid->Remove( collisionGridRight, collisionGridBottom, this );
}

SDL_Rect Bullet::GetCollisionRect( )
{
	if( deathFramesLeft > 0 )
		return { 0, 0, 0, 0 };

	SDL_Rect rect;

	rect.x = (int) xPosition;
	rect.y = (int) yPosition;
	rect.w = bulletType->collisionWidth;
	rect.h = bulletType->collisionHeight;

	return rect;
}

const DrawData Bullet::GetDrawData( )
{
	DrawData tmpData;
	SDL_Rect clipArea = SDL_Rect( bulletType->graphicRect );

	tmpData.blitZone.x =	xPosition;
	tmpData.blitZone.y =	yPosition;
	tmpData.blitZone.w =	clipArea.w;
	tmpData.blitZone.h =	clipArea.h;

	tmpData.clipZone =	clipArea;

	tmpData.angle =			angle / (float) (M_PI / 180);

	tmpData.shouldRotate =	bulletType->shouldRotate;
	tmpData.shouldStretch = true;

	if( bulletType->addBlend )
		tmpData.blendMode = BLEND_ADD;
	else
		tmpData.blendMode = BLEND_ALPHA;

	if( deathFramesLeft >= 0 )
		tmpData.alpha = 0.1f + (0.9f * deathFramesLeft / DEATH_FRAMES);
	else
		tmpData.alpha = 1.0f;

	return tmpData;
}