#pragma once

#include "LuaFunctions.h"
#include "LuaInclude.h"

namespace LuaScripting
{
	class LuaClassWrapper
	{
		public:
		virtual void ExposeFunctions( lua_State* script ) = 0;
	};
}