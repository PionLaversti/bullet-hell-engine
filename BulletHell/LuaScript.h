#pragma once

#include "LuaInclude.h"

namespace LuaScripting
{
	class LuaScript
	{
		public:
		LuaScript( std::string scriptFileName );

		void Preprocessing( );
		void Initialize( );
		void Update( );
		void Render( );
		void ShutDown( );

		void CallFunction( const std::string functionName );

		template<class Entity> void RegisterEntity( Entity* entity, std::string name );

		private:
		lua_State* script;
	};

	template<class Entity>
	void LuaScript::RegisterEntity( Entity* entity, std::string name )
	{
		luabridge::push( script, entity );
		lua_setglobal( script, name.c_str( ) );
	}
}