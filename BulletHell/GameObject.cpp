#include <SDL_rect.h>
#include "CollisionGrid.h"
#include "GameObject.h"

using namespace GameObjects;

GameObject::GameObject( )
{
	collisionXOffset = 0;
	collisionYOffset = 0;
}

void GameObject::Initialize( )
{
	active = true;
}
void GameObject::Update( )
{

}
void GameObject::ShutDown( )
{
	active = false;
}
void GameObject::Destroy( )
{
	active = false;
}

SDL_Rect GameObject::GetCollisionRect()
{
	SDL_Rect tmpRect;
	tmpRect.x = (int) (xPosition + collisionXOffset);
	tmpRect.y = (int) (yPosition + collisionYOffset);
	tmpRect.w = (int) collisionWidth;
	tmpRect.h = (int) collisionHeight;

	return tmpRect;
}

void GameObject::SetCollisionSize( float width, float height )
{
	collisionXOffset = 0;
	collisionYOffset = 0;
	collisionWidth = width;
	collisionHeight = height;
}

void GameObject::SetCollisionRect( float X, float Y, float width, float height )
{
	collisionXOffset = X;
	collisionYOffset = Y;
	collisionWidth = width;
	collisionHeight = height;
}

void GameObject::MoveBy( float xDelta, float yDelta )
{
	xPosition += xDelta;
	yPosition += yDelta;
}