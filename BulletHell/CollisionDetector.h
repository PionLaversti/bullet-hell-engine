#pragma once

#include <vector>

namespace GameObjects
{
	namespace Projectiles
	{
		class Bullet;
		class Bullet_Player;
		class Laser;
	}
}

namespace GameSystems
{
	class CollisionGrid;

	class CollisionDetector
	{
		public:
		static void Initialize( const int screenWidth, const int screenHeight );

			// Provides a list of bullets and a target to check against. Will use appropriate type of collision detection.
		static bool Intersects( const std::vector<GameObjects::Projectiles::Bullet *> * _bulletList, const SDL_Rect _target );
			// Provides a list of lasers and a target to check against. Will use appropriate type of collision detection.
		static bool Intersects( const std::vector<GameObjects::Projectiles::Laser *> * _laserList, const SDL_Rect _target );

		static bool IsPlayerHitByBullet( const SDL_Rect playerRect );
		static std::vector< GameObjects::Projectiles::Bullet_Player* > GetBulletsHittingEnemy( const SDL_Rect playerRect );

		static std::vector< GameObjects::Projectiles::Bullet* > GetBulletCollisions( const std::vector<GameObjects::Projectiles::Bullet*> * bulletList, const SDL_Rect target );

		private:
			// Axis-aligned bounding box collision. Basic collision detection.
		static bool Intersects_AABB( GameObjects::Projectiles::Bullet* object, const SDL_Rect _target );
			// Linear collision. Used for lasers. Detects collision along a line.
		static bool Intersects_Linear( GameObjects::Projectiles::Laser* _las, const SDL_Rect _target );

		static CollisionGrid enemyBulletCollisionGrid;
		static CollisionGrid playerBulletCollisionGrid;
	};
}