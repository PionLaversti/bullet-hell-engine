#include <SDL_Mixer.h>
#include "SoundWrapper.h"

int SoundWrapper::maxChannels;
int SoundWrapper::lastChannel;

void SoundWrapper::Initialize()
{	
	lastChannel = 1;

	maxChannels = 32;
	Mix_AllocateChannels(maxChannels);
}

//	--	--	--	--	--	--	--	--	--	--	--	--	--	--	--
//	 bool play_music()
//	Plays the specified music file.
//	If one is already playing, Stop it and play another.
//	--	--	--	--	--	--	--	--	--	--	--	--	--	--	--
bool SoundWrapper::PlayMusic( Mix_Music* musicFile )
{
	try
	{
		Mix_PlayMusic( musicFile, -1 );
		return true;
	}
	catch(...)
	{
		return false;
	}
}

//	--	--	--	--	--	--	--	--	--	--	--	--	--	--	--
//	 void pause_music()
//	Pauses the currently-playing music.
//	--	--	--	--	--	--	--	--	--	--	--	--	--	--	--
void SoundWrapper::PauseMusic()
{
	if( Mix_PlayingMusic() )
		Mix_PauseMusic();
}

//	--	--	--	--	--	--	--	--	--	--	--	--	--	--	--
//	 void resume_music()
//	Resume a paused music.
//	--	--	--	--	--	--	--	--	--	--	--	--	--	--	--
void SoundWrapper::ResumeMusic()
{
	if( Mix_PausedMusic() )
		Mix_ResumeMusic();
}

//	--	--	--	--	--	--	--	--	--	--	--	--	--	--	--
//	 void stop_music()
//	Stops the currently-playing music.
//	--	--	--	--	--	--	--	--	--	--	--	--	--	--	--
void SoundWrapper::StopMusic()
{
	if( Mix_PlayingMusic() || Mix_PausedMusic() )
		Mix_HaltMusic();
}

//	--	--	--	--	--	--	--	--	--	--	--	--	--	--	--
//	 void play_SE()
//	Plays the specified sound effect.
//	--	--	--	--	--	--	--	--	--	--	--	--	--	--	--
bool SoundWrapper::PlaySE( Mix_Chunk* soundEffect )
{
	try
	{
		Mix_PlayChannel( -1, soundEffect, 0 );
		return true;
	}
	catch(...)
	{
		return false;
	}
}