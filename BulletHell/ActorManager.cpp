#include "ActorManager.h"

#include "Actor.h"
#include "Enemy.h"
#include "Player.h"

using namespace GameObjects;
using namespace GameSystems;

Player* ActorManager::activePlayer;
std::list<Enemy*> ActorManager::enemies;
std::list< GameObjects::Enemy* >::iterator ActorManager::enemyIter;

ActorManager::ActorManager( )
{
	activePlayer = nullptr;
}

void ActorManager::SpawnEnemy( const std::string scriptFileName )
{
	Enemy* enemy = new Enemy( scriptFileName );
	enemies.push_back( enemy );

	enemy->Preprocessing( );
	enemy->Initialize( );
}

void ActorManager::SpawnEnemyAt( const std::string scriptFileName, const int X, const int Y )
{
	Enemy* enemy = new Enemy( scriptFileName );
	enemies.push_back( enemy );

	enemy->Preprocessing( );
	enemy->Initialize( );

	enemy->SetX( (float) X );
	enemy->SetY( (float) Y );
}

void ActorManager::SpawnPlayer( const std::string scriptFileName )
{
	activePlayer = new Player( scriptFileName );
	activePlayer->Preprocessing( );
}

void ActorManager::Initialize( )
{
	if( activePlayer != nullptr )
		activePlayer->Initialize( );

	enemyIter = enemies.begin( );

	while( enemyIter != enemies.end( ) )
	{
		if( (*enemyIter) != nullptr )
			(*enemyIter)->Initialize( );

		enemyIter ++;
	}
}

void ActorManager::Update( )
{
	if( activePlayer != nullptr )
		activePlayer->Update( );

	enemyIter = enemies.begin( );

	while( enemyIter != enemies.end( ) )
	{
		if( (*enemyIter)->IsActive( ) )
			(*enemyIter)->Update( );
		else
		{
			delete (*enemyIter);
			(*enemyIter) = nullptr;

			enemies.remove( *enemyIter );
		}

		enemyIter ++;
	}
}

void ActorManager::Render( )
{
	if( activePlayer != nullptr )
		activePlayer->Render( );

	enemyIter = enemies.begin( );

	while( enemyIter != enemies.end( ) )
	{
		if( (*enemyIter)->IsActive( ) )
			(*enemyIter)->Render( );

		enemyIter ++;
	}
}

void ActorManager::ShutDown( )
{
	if( activePlayer != nullptr )
	{
		activePlayer->ShutDown( );

		delete activePlayer;
		activePlayer = nullptr;
	}

	enemyIter = enemies.begin( );

	while( enemyIter != enemies.end( ) )
	{
		if( (*enemyIter)->IsActive( ) )
			(*enemyIter)->ShutDown( );

		delete (*enemyIter);

		enemyIter ++;
	}
}

Enemy* ActorManager::GetEnemyByID( const int ID )
{
	enemyIter = enemies.begin( );

	while( enemyIter != enemies.end( ) )
	{
		if( (*enemyIter)->GetID( ) == ID )
			return (*enemyIter);

		enemyIter ++;
	}
	
	return nullptr;
}

bool ActorManager::HasZeroEnemiesOnScreen( )
{
	enemyIter = enemies.begin( );

	while( enemyIter != enemies.end( ) )
	{
		if( (*enemyIter)->IsActive( ) )
			return false;

		enemyIter ++;
	}

	return true;
}

void ActorManager::DestroyAllEnemiesOnScreen( )
{
	enemyIter = enemies.begin( );

	while( enemyIter != enemies.end( ) )
	{
		if( (*enemyIter)->IsActive( ) )
			(*enemyIter)->ShutDown( );

		enemyIter ++;
	}
}