#include "ResourceManager.h"

#include <SDL_Mixer.h>
#include <SDL_ttf.h>

#include "Texture.h"

std::map< std::string, Mix_Chunk* >		ResourceManager::lib_SE;
std::map< std::string, Mix_Music* >		ResourceManager::lib_music;
std::map< std::string, Texture* >		ResourceManager::lib_textures;
std::map< std::string, TTF_Font* >		ResourceManager::lib_fonts;

	//	--	-- 
	//	IMAGES
	//	--	--

//	Retrieves the image with the specified filename.
Texture* ResourceManager::GetTexture( std::string _key )
{
	return lib_textures[ _key ];
}

//	Loads an image with the specified filename.
bool ResourceManager::LoadTexture( std::string _filename )
{
	if (lib_textures[_filename])
		return true;

	Texture* tmp = new Texture();
	
	bool success = tmp->LoadTextureFromFile(_filename);

	if( !success )
		return false;

	lib_textures[_filename] = tmp;

	if( lib_textures[_filename] )
	{
		tmp = nullptr;
		return true;
	}
	else
	{
		lib_textures.erase(_filename);
		tmp = nullptr;
		return false;
	}
}

//	Releases an image with the specified filename.
void ResourceManager::ReleaseTexture( std::string _key )
{
	assert( lib_textures.count( _key ) > 0 );

		// Free the texture
	(lib_textures[ _key ])->FreeTexture();
		// Remove from our map
	lib_textures.erase( _key );
}

	//	--	-- 
	//	FONTS
	//	--	--

//	Retrieves the font with the specified filename.
TTF_Font* ResourceManager::GetFont( std::string _key )
{
	return lib_fonts[ _key ];
}

//	Loads a font with the specified filename, at the specified
//	 font weight.
bool ResourceManager::LoadFont( std::string _filename, int _weight )
{
	if (lib_fonts[_filename])
		return true;

	lib_fonts[ _filename ] = TTF_OpenFont( _filename.c_str(), _weight );

	if( lib_fonts[ _filename ] )
		return true;
	else
	{
		lib_fonts.erase( _filename );
		return false;
	}
}

//	Releases the font with the specified filename.
void ResourceManager::ReleaseFont( std::string _key )
{
		// Free the texture
	TTF_CloseFont( lib_fonts[ _key ] );
		// Remove from our map
	lib_fonts.erase( _key );
}

	//	--	-- 
	//	MUSIC
	//	--	--

//	Retrieves the music with the specified filename.
Mix_Music* ResourceManager::GetMusic( std::string _key )
{
	return lib_music[ _key ];
}

//	Loads a music file with the specified filename.
bool ResourceManager::LoadMusic( std::string _filename )
{
	if (lib_music[_filename])
		return true;

	lib_music[ _filename ] = Mix_LoadMUS( _filename.c_str() );

	if( lib_music[ _filename ] )
		return true;
	else
	{
		lib_music.erase( _filename );
		return false;
	}
}

//	Releases the music with the specified filename.
void ResourceManager::ReleaseMusic( std::string _key )
{
		// Free the texture
	Mix_FreeMusic( lib_music[ _key ] );
		// Remove from our map
	lib_music.erase( _key );
}

	//	--	-- 
	//	SOUND EFFECTS
	//	--	--

//	Retrieves the sound effect with the specified filename.
Mix_Chunk* ResourceManager::GetSE( std::string _key )
{
	return lib_SE[ _key ];
}

//	Loads a sound effect with the specified filename.
bool ResourceManager::LoadSE( std::string _filename )
{
	if (lib_SE[_filename])
		return true;

	lib_SE[ _filename ] = Mix_LoadWAV( _filename.c_str() );

	if( lib_SE[ _filename ] )
		return true;
	else
	{
		lib_SE.erase( _filename );
		return false;
	}
}

//	Releases the font with the specified filename.
void ResourceManager::ReleaseSE( std::string _key )
{
	assert( lib_SE.count( _key ) > 0 );

		// Free the sound effect
	Mix_FreeChunk( lib_SE[ _key ] );
		// Remove from our map
	lib_SE.erase( _key );
}

	//	--	-- 
	//	ETC
	//	--	--

//	Releases all assets currently loaded.
void ResourceManager::release_all()
{
		// Create iterators to move through the maps.
	std::map< std::string, Texture* >::iterator text_iter;
	std::map< std::string, TTF_Font* >::iterator	font_iter;
	std::map< std::string, Mix_Music* >::iterator	music_iter;
	std::map< std::string, Mix_Chunk* >::iterator	SE_iter;
	
	//	-- TEXTURES
	for( text_iter = lib_textures.begin(); text_iter != lib_textures.end(); ++ text_iter )
	{
			// Loop over the map, releasing any valid textures.
		if( text_iter->second != NULL )
			text_iter->second->FreeTexture();
	}

	lib_textures.clear();

	//	-- FONTS
	for( font_iter = lib_fonts.begin(); font_iter != lib_fonts.end(); ++ font_iter )
	{
		if( font_iter->second != NULL )
			TTF_CloseFont( font_iter->second );
	}

	lib_fonts.clear();

	//	-- MUSIC
	for( music_iter = lib_music.begin(); music_iter != lib_music.end(); ++ music_iter )
	{
		if( music_iter->second != NULL )
			Mix_FreeMusic( music_iter->second );
	}

	lib_music.clear();

	//	-- SOUND EFFECTS
	for( SE_iter = lib_SE.begin(); SE_iter != lib_SE.end(); ++ SE_iter )
	{
		if( SE_iter->second != NULL )
			Mix_FreeChunk( SE_iter->second );
	}

	lib_SE.clear();

	assert( lib_textures.empty() );
	assert( lib_fonts.empty() );
	assert( lib_music.empty() );
	assert( lib_SE.empty() );
}