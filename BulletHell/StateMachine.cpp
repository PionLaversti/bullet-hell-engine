#include "StateMachine.h"

#include "GameState.h"
#include "State_InGame.h"

using namespace GameSystems::StateMachineSystem;

GameState* StateMachine::currentState;
int StateMachine::currentStateID;
int StateMachine::nextStateID;

void StateMachine::Initialize()
{
	currentState = new State_InGame( 640, 480 ); 
	currentStateID =	STATE_INGAME;
	nextStateID =		STATE_NULL;
}

	// Changes the application's next state if needed.
void StateMachine::ChangeState()
{
	nextStateID = currentState->GetNextState();

		// STATE_NULL : No change needed.
	if( nextStateID == STATE_NULL )
		return;

		// Delete the current state.
	if( currentState != NULL && nextStateID != STATE_EXIT )
		delete currentState;

		// Finally change the game's state.
	switch( nextStateID )
	{
		case STATE_INGAME:
			currentState = new State_InGame( 640, 480 ); break;
	}

	currentStateID = nextStateID;

	nextStateID = STATE_NULL;
}
	
	// State-machine methods.
void StateMachine::HandleEvents()
{
	currentState->HandleEvents();
}
void StateMachine::Update()
{
	currentState->HandleLogic();
}
void StateMachine::Render()
{
	currentState->Render();
}

bool StateMachine::HasStopped( )
{
	return currentStateID == GameStateType::STATE_EXIT;
}