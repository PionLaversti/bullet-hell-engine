#pragma once

#include <string>

#include "LuaClassWrapper.h"

namespace LuaScripting
{
	class LuaWrapper_Graphics : public LuaClassWrapper
	{
		public:
		static void ExposeFunctions( lua_State* script );

		private:
		static void LoadTexture( const std::string fileName );
		static void Render( const std::string fileName, const float X, const float Y );
		static void RenderToSize( const std::string fileName, const float X, const float Y, const float width, const float height );
		static void RenderInRect( const std::string fileName, const float left, const float top, const float right, const float bottom );
		static void UnloadTexture( const std::string fileName );
	};
}