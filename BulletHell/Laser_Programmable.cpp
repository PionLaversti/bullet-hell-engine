#include "Laser_Programmable.h"

#include "ProjectileDataManager.h"
#include "ProjectileType.h"

using namespace GameObjects::Projectiles;
using namespace GameSystems;
using namespace std;

ProgrammableLaser::ProgrammableLaser( ) : Laser( )
{
	isActive = true;
}

void ProgrammableLaser::SetGraphic( string bulTypeAlias )
{
	laserTypeAlias = bulTypeAlias;
	laserType = ProjectileDataManager::FindByAlias( bulTypeAlias );
}

void ProgrammableLaser::Update( )
{
	Laser::Update( );

	startX += xSpeed;
	startY += ySpeed;

	if( angAccel != 0 )
		angle += angAccel;

	if( xAccel != 0 )
	{
		xSpeed += xAccel;

		if( (xAccel > 0 && xMaximumSpeedLimit) || (xAccel < 0 && xMinimumSpeedLimit) )
			xSpeed = xMaxSpeed;
	}
	if( yAccel != 0 )
	{
		ySpeed += yAccel;

		if( (yAccel > 0 && yMaximumSpeedLimit) || (yAccel < 0 && yMinimumSpeedLimit) )
			ySpeed = yMaxSpeed;
	}
}
void ProgrammableLaser::SetXMaxSpeed( float newXMaxSpeed )
{
	if( xMaxSpeed < xSpeed )
	{
		xMinimumSpeedLimit = true;
		xMaximumSpeedLimit = false;
	}
	else
	{
		xMinimumSpeedLimit = false;
		xMaximumSpeedLimit = true;
	}

	xMaxSpeed = newXMaxSpeed;
}
void ProgrammableLaser::SetYMaxSpeed( float newYMaxSpeed )
{
	if( yMaxSpeed < ySpeed )
	{
		yMinimumSpeedLimit = true;
		yMaximumSpeedLimit = false;
	}
	else
	{
		yMinimumSpeedLimit = false;
		yMaximumSpeedLimit = true;
	}

	yMaxSpeed = newYMaxSpeed;
}

void ProgrammableLaser::SetXYSpeed( const float newXSpeed, const float newYSpeed )
{
	xSpeed = newXSpeed;
	ySpeed = newYSpeed;
}
void ProgrammableLaser::SetXYAccel( const float newXAccel, const float newYAccel )
{
	xAccel = newXAccel;
	yAccel = newYAccel;
}
void ProgrammableLaser::SetXYMaxSpeed( const float newXMaxSpeed, const float newYMaxSpeed )
{
	SetXMaxSpeed( newXMaxSpeed );
	SetYMaxSpeed( newYMaxSpeed );
}