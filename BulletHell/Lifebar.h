#pragma once

#include <string>

namespace LuaScripting
{
	class LuaScript;
}

namespace GameObjects
{
	class Enemy;

	class Lifebar
	{
		public:
		Lifebar( const std::string scriptName );

		virtual void Preprocessing( );
		virtual void Initialize( );
		virtual void Update( );
		virtual void Render( );
		virtual void ShutDown( );

		virtual void LinkEnemy( Enemy* linkedEnemy ) { enemy = linkedEnemy; }

		virtual float GetCurrentLife( ) { return currentLife; }
		virtual float GetMaximumLife( ) { return maximumLife; }
		virtual float GetLifeRatio( ) { return currentLife / maximumLife; }
		virtual bool IsActive( ) { return active; }

		private:
		GameObjects::Enemy* enemy;
		LuaScripting::LuaScript* script;

		float currentLife;
		float maximumLife;
		bool active;
	};
}